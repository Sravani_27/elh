package com.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.User;
import com.service.UserService;
import com.service.UserServiceImplementation;

@RestController
public class UserController {

	UserService service = new UserServiceImplementation();

	@PostMapping(value = "adduser")
	public ResponseEntity<User> addUser(@RequestBody User user) {

		Boolean userValidation = service.validateUser(user);
		if (userValidation) {
			System.out.println("User ID:" + user.getUserId());
			System.out.println("User Name: " + user.getUserName());
			System.out.println("User password: " + user.getUserPassword());
		}
		ResponseEntity<User> entity = new ResponseEntity<User>(user, HttpStatus.CREATED);

		return entity;
	}

	@GetMapping(value = "readuserid/{userid}")
	public ResponseEntity<User> readUserByUserId(@PathVariable("userid") int userId) {
		ResponseEntity<User> responseEntity = null; // new ResponseEntity<User>(body, status)
		User user = null;
		Boolean check = service.validateUserById(userId);
		if (check) {
			if (userId == 110) {
				user = new User(110, "Carrot", "getlost");
			}
			if (userId == 120) {
				user = new User(120, "Spinach", "Downdown");
			}
			if (userId == 140) {
				user = new User(140, "Cola", "Upupup");
			}
		}
		return new ResponseEntity<User>(user, HttpStatus.FOUND);
	}

	@GetMapping(value = "readusername/{username}")
	public ResponseEntity<User> readUserByUserName(@PathVariable("username") String userName) {
		ResponseEntity<User> responseEntity = null; // new ResponseEntity<User>(body, status)
		User user = null;
		Boolean check = service.validateUserByName(userName);
		// System.out.println("service");
		if (check) {
			if (userName.equalsIgnoreCase("carrot")) {
				user = new User(110, "Carrot", "getlost");
			}
			if (userName.equalsIgnoreCase("spinach")) {
				user = new User(120, "Spinach", "Downdown");
			}
			if (userName.equalsIgnoreCase("cocola")) {
				user = new User(140, "CoCola", "Upupup");
			}
		}
		return new ResponseEntity<User>(user, HttpStatus.FOUND);
	}

	@PutMapping(value = "updatename/{changeusername}")
	public ResponseEntity<User> updateUser(@RequestBody User user,
			@PathVariable("changeusername") String changeUserName) {

		user.setUserName(changeUserName);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete/{deleteuser}")
	public ResponseEntity<Boolean> deleteEmployeeById(@RequestBody User user, @PathVariable("deleteuser") int empId) {
		System.out.println("Deleted..!!");
		return new ResponseEntity<Boolean>(true, HttpStatus.GONE);
	}

}
