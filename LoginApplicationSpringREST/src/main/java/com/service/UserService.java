package com.service;

import com.model.User;

public interface UserService {

	public abstract Boolean validateUserByName(String userName);

	public abstract Boolean validateUser(User user);

	public abstract Boolean validateUserById(int userId);

}
