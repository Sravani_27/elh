package com.service;

import com.model.User;

public class UserServiceImplementation implements UserService {
	User user = new User();

	@Override
	public Boolean validateUserByName(String userName) {
		Boolean validate = false;
		if (userName.length() >= 6 || userName.length() <= 32) {

			validate = true;
		}
		return validate;
	}

	@Override
	public Boolean validateUser(User user) {
		Boolean validate = false;
		int length = String.valueOf(user.getUserId()).length();
		if (user.getUserName().length() >= 6 && user.getUserName().length() < 32) {
			validate = true;
		} else if (length >= 1 && length <= 32) {
			validate = true;
		} else {
			validate = false;
		}

		return validate;
	}

	@Override
	public Boolean validateUserById(int userId) {
		Boolean validate = false;
		int length = String.valueOf(user.getUserId()).length();
		if (length >= 1 || length >= 32) {
			validate = true;
		}
		return validate;
	}

}
