package com.model;

import java.io.Serializable;

public class Student implements Serializable {

	private int studentNumber;
	private String studentName;
	private int studentMark;

	public Student() {
		super();
	}

	public Student(int studentNumber, String studentName, int studentMark) {
		super();
		this.studentNumber = studentNumber;
		this.studentName = studentName;
		this.studentMark = studentMark;
	}

	public int getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(int studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentMark() {
		return studentMark;
	}

	public void setStudentMark(int studentMark) {
		this.studentMark = studentMark;
	}

}
