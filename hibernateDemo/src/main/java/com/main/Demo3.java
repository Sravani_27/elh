package com.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Student;

public class Demo3 {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Student student = session.load(Student.class, 40);
		try {
			System.out.println("student Details:" + student.getStudentNumber());
			System.out.println(student.getStudentName());
			System.out.println(student.getStudentMark());

		} catch (org.hibernate.ObjectNotFoundException e) {
			System.out.println("Invalid");
		}

	}

}
