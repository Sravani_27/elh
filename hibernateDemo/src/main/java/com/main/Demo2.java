package com.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Student;

public class Demo2 {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Student student = session.get(Student.class, 40);
		if (student != null) {
			System.out.println("student Details:" + student.getStudentNumber());
			System.out.println(student.getStudentName());
			System.out.println(student.getStudentMark());
		} else {
			System.out.println("Invalid");
		}
	}

}
