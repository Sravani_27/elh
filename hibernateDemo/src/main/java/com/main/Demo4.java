package com.main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.mapping.Set;

import com.model.Student;
import com.mysql.cj.Query;

public class Demo4 {

	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		// HQL
		String hqlRead = "from Student where studentNumber>0 ORDER BY studentNumber DESC";
		// "from Student"
		// "from Student" "from Student where studentNumber = :abc"
		// "from Student where studentNumber>0 ORDER BY studentNumber DESC"
		org.hibernate.query.Query<Student> query = session.createQuery(hqlRead);
		//query.setInteger("abc", 21);

		List<Student> list = query.list();
		for (Student student : list) {
			System.out.println("student Details:");
			System.out.println(student.getStudentNumber());
			System.out.println(student.getStudentName());
			System.out.println(student.getStudentMark());
		}
		System.out.println("End of the main");
	}

}
