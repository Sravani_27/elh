package com.main;

import javax.xml.transform.TransformerConfigurationException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Student;

public class Demo1 {

	public static void main(String[] args) {
		// StandardServiceRegistry ssr=new
		// StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml").build();
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Transaction transaction = session.beginTransaction();
		Student student = new Student(12, "park seo joon", 94);
		session.save(student);// insert into table values
		transaction.commit();
		// transaction.rollback();
		System.out.println("End of the main");
	}

}
