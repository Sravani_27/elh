package com.consumerservices.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "controller")
public class ConsumerController {

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/consume")
	public ResponseEntity<String> getFromProducer() {
		// fetch data from producer
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		String fromProducer = restTemplate
				.exchange("http://localhost:9090/producer/world", HttpMethod.GET, entity, String.class).getBody();
		return new ResponseEntity<String>(fromProducer, HttpStatus.OK);
	}
}
