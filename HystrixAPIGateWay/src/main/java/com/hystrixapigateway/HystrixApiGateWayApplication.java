package com.hystrixapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEurekaClient
@EnableHystrix
@EnableHystrixDashboard
public class HystrixApiGateWayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HystrixApiGateWayApplication.class, args);
	}

}
