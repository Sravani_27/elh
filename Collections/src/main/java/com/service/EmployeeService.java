package com.service;

import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

public interface EmployeeService {

	public abstract Employee searchByEmployeeName(String employeeName, Set<Employee> employees) throws EmployeeException;

	public abstract Employee searchByEmployeeNumber(int employeeNumber, Set<Employee> employees) throws EmployeeException;

	public abstract Employee searchByEmployeeSalary(float employeeSalary, Set<Employee> employees) throws EmployeeException;

}
