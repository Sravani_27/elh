package com.service;

import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee searchByEmployeeName(String employeeName, Set<Employee> employees) {

		Employee employee1 = null;
		for (Employee listEmployee : employees) {
			if (listEmployee.getEmployeeName().equals(employeeName)) {
				employee1 = listEmployee;
			} else {
				EmployeeException employeeException = new EmployeeException("Not found data");
				System.err.println(employeeException.getMessage());
			}
		}

		return employee1;
	}

	@Override
	public Employee searchByEmployeeNumber(int employeeNumber, Set<Employee> employees) {
		Employee employee = null;
		for (Employee listEmployee : employees) {
			if (listEmployee.getEmployeeNumber() == employeeNumber) {
				employee = listEmployee;
			} else {
				EmployeeException employeeException = new EmployeeException("Not found data");
				System.err.println(employeeException.getMessage());
			}
		}

		return employee;

	}

	@Override
	public Employee searchByEmployeeSalary(float employeeSalary, Set<Employee> employees) {
		Employee employee2 = null;
		for (Employee listEmployee : employees) {
			if (listEmployee.getSalary() == employeeSalary) {
				employee2 = listEmployee;
			} else {
				EmployeeException employeeException = new EmployeeException("Not found data");
				System.err.println(employeeException.getMessage());
			}
		}

		return employee2;
	}

}
