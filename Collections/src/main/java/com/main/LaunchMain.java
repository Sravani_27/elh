package com.main;

import java.util.HashSet;
import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class LaunchMain {

	public static void main(String[] args) {

		Employee employee1 = new Employee(1234, "Minnie", 2300f);
		Employee employee2 = new Employee(1111, "choi min", 2000f);
		Employee employee3 = new Employee(1112, "Donald", 3000f);

		Set<Employee> employees = new HashSet<>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);

		EmployeeService employeeService = new EmployeeServiceImpl();
		try {
			// employeeService.searchByEmployeeNumber(1112, employees);
			Employee employeeRefer = employeeService.searchByEmployeeName("Donald", employees);
			System.out.println(employeeRefer.getEmployeeName());
			System.out.println(employeeRefer.getEmployeeNumber());
			System.out.println(employeeRefer.getSalary());
		} catch (EmployeeException e) {

			e.printStackTrace();
		}

	}

}
