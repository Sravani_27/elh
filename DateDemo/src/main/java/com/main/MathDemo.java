package com.main;

public class MathDemo {

	public static void main(String[] args) {
		System.out.println(Math.max(12, 8));
		System.out.println(Math.min(3, 0));
		System.out.println(Math.sqrt(22));
		System.out.println(Math.addExact(11, 98));
		System.out.println(Math.round(9.33345f));
	}

}
