package com.thymleafdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thymleafdemo.model.User;

@Controller
public class UserController {
	
	@GetMapping("/first")
	public String firstMethod() {
		return"index";
	}
	
	@PostMapping("/save")
	public ModelAndView secondMethod(@ModelAttribute User user) { //in @Controller we use @@ModelAttribute instead of @RequestBody
		  
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		//modelAndView.addObject(user);
		modelAndView.addObject("userData", user);
		 return modelAndView;
	}
}
