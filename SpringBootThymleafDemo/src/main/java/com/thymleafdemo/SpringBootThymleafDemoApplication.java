package com.thymleafdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootThymleafDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootThymleafDemoApplication.class, args);
	}

}
