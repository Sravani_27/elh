package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnection {
	static Connection connection = null;
	static {
		try {
			String driverName = "com.mysql.cj.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/elh";
			Class.forName(driverName);
			connection = DriverManager.getConnection(url, "root", "root");
			System.out.println(connection != null ? "connection established" : "connection failed");
		} catch (ClassNotFoundException cnfe) {
			System.out.println("There is no respective jars : " + cnfe.getMessage());
		} catch (SQLException se) {// Catching SQL Exception
			System.out.println("SQL Exception :" + se.getMessage());
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static Connection getConnection() {
		return connection;
	}
}
