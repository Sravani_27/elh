package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee createEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> readAllEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeByNumber(int EmployeeNumber) {

		ResultSet resultSet;
		String query = "SELECT * FROM employee WHERE emp_id = ?;";

		Connection connection = MysqlConnection.getConnection();// got connection to elh
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, EmployeeNumber);
			resultSet = preparedStatement.executeQuery();
			// iteration
			while (resultSet.next()) {
				System.out.println("Employee Number: " + resultSet.getInt("emp_id"));
				System.out.println("Employee First Name: " + resultSet.getString("first_name"));
				System.out.println("Employee LastName: " + resultSet.getString("last_name"));
				System.out.println("Employee Location: " + resultSet.getString("city"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Employee readEmployeeByName(String EmployeeFname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteEmployeeByNumber(int employeeNumber) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteEmployeeByName(int employeeFname) {
		// TODO Auto-generated method stub
		return 0;
	}

}
