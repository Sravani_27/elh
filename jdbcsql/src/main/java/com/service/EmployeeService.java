package com.service;

import java.util.List;

import com.exception.EmployeeException;
import com.model.Employee;

public interface EmployeeService {

	public abstract Employee createEmployee(Employee employee);

	public abstract List<Employee> readAllEmployee();

	public abstract Employee readEmployeeByNumber(int EmployeeNumber) throws EmployeeException;

	public abstract Employee readEmployeeByName(String EmployeeFname);

	public abstract Employee updateEmployee(Employee employee);

	public abstract int deleteEmployeeByNumber(int employeeNumber);

	public abstract int deleteEmployeeByName(int employeeFname);
}
