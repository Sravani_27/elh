package com.service;

import java.util.List;

import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;
import com.exception.EmployeeException;
import com.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee createEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> readAllEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeByNumber(int EmployeeNumber) throws EmployeeException {

		int length = String.valueOf(EmployeeNumber).length();

		if (length > 0) {

			EmployeeDao employeeDao = new EmployeeDaoImpl();
			employeeDao.readEmployeeByNumber(EmployeeNumber);
		} else {

			throw new EmployeeException("Something is Wrong");

		}
		return null;
	}

	@Override
	public Employee readEmployeeByName(String EmployeeFname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteEmployeeByNumber(int employeeNumber) {

		return 0;
	}

	@Override
	public int deleteEmployeeByName(int employeeFname) {
		// TODO Auto-generated method stub
		return 0;
	}

}
