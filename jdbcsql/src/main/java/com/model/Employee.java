package com.model;

public class Employee {
	private int employeeNumber;
	private String employeeFname;
	private String employeeLname;
	private String employeePlace;

	public Employee() {
		super();

	}

	public Employee(int employeeNumber, String employeeFname, String employeeLname, String employeePlace) {
		super();
		this.employeeNumber = employeeNumber;
		this.employeeFname = employeeFname;
		this.employeeLname = employeeLname;
		this.employeePlace = employeePlace;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getEmployeeFname() {
		return employeeFname;
	}

	public void setEmployeeFname(String employeeFname) {
		this.employeeFname = employeeFname;
	}

	public String getEmployeeLname() {
		return employeeLname;
	}

	public void setEmployeeLname(String employeeLname) {
		this.employeeLname = employeeLname;
	}

	public String getEmployeePlace() {
		return employeePlace;
	}

	public void setEmployeePlace(String employeePlace) {
		this.employeePlace = employeePlace;
	}

}
