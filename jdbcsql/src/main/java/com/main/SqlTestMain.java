package com.main;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class SqlTestMain {

	public static void main(String[] args) {
		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee data;

		try {
			data = employeeService.readEmployeeByNumber(12);
			if (data != null) {
				System.out.println("Employee number: " + data.getEmployeeNumber());
				System.out.println("Employee first name: " + data.getEmployeeFname());
				System.out.println("Employee last name: " + data.getEmployeeLname());
				System.out.println("Employee location: " + data.getEmployeePlace());
			} else {
				System.err.println("Data is not available");
			}

		} catch (EmployeeException e) {

			e.printStackTrace();
		}
		{

		}
	}
}
