package com.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.model.Student;

public class StudentDaoImpl implements StudentDao {

	private EntityManager entityManager;// has a relation

	public StudentDaoImpl(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	public Optional<Student> save(Student student) {
		try {

			entityManager.persist(student);

			return Optional.of(student);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();

	}

	@Override
	public Optional<Student> findByStudentId(Integer studentId) {
		Student student = entityManager.find(Student.class, studentId);
		return student != null ? Optional.of(student) : Optional.empty();
	}

	@Override
	public Optional<Student> findByStudentName(String studentName) {
		Student student = entityManager.find(Student.class, studentName);
		return student != null ? Optional.of(student) : Optional.empty();
	}

	@Override
	public Optional<Student> update(Student student) {
		try {
			Student loadStudent = entityManager.find(Student.class, student.getStudentId());

			entityManager.merge(student);

			return Optional.of(student);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	@Override
	public Integer deleteByStudentId(Integer studentId) {
		Student student = entityManager.find(Student.class, studentId);

		entityManager.remove(student);

		return studentId;
	}

	@Override
	public List<Student> findAll() {
		return entityManager.createQuery("from Student").getResultList();
	}

}
