package com.dao;

import java.util.List;
import java.util.Optional;

import com.model.Student;

public interface StudentDao {

	public abstract Optional<Student> save(Student student);

	public abstract Optional<Student> findByStudentId(Integer studentId);

	public abstract Optional<Student> findByStudentName(String studentName);
	
	public abstract List<Student> findAll();

	public abstract Optional<Student> update(Student student);

	public abstract Integer deleteByStudentId(Integer studentId);

}
