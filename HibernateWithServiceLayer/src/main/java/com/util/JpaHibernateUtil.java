package com.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaHibernateUtil {

	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;

	static {
		entityManagerFactory = Persistence.createEntityManagerFactory("demo_persist_xml");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public static EntityManager getConnection() {
		return entityManager;
	}

}
