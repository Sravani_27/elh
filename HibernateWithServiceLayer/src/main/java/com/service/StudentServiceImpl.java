package com.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.dao.StudentDao;
import com.dao.StudentDaoImpl;
import com.exception.StudentException;
import com.model.Student;
import com.util.JpaHibernateUtil;

public class StudentServiceImpl implements StudentService {

	public EntityManager entityManager = JpaHibernateUtil.getConnection();
	StudentDao studentDao = new StudentDaoImpl(entityManager);

	@Override
	public Optional<Student> save(Student student) {
		// System.out.println("Inside service impl save()");

		Optional<Student> optional = null;
		boolean checkStudent = validateStudent(student);
		if (checkStudent) {
			try {

				entityManager.clear();
				entityManager.getTransaction().begin();
				optional = studentDao.save(student);
				entityManager.getTransaction().commit();
			} catch (Exception e) {
				System.err.println(e);
				entityManager.getTransaction().rollback();
			}
			return optional;
		} else {
			return optional;
		}
	}

	@Override
	public Optional<Student> findByStudentId(Integer studentId) {
		Student student = new Student();
		student.setStudentId(studentId);
		boolean flag = validateStudent(student);
		Optional<Student> opt = null;
		if (flag) {
			opt = studentDao.findByStudentId(studentId);
			return opt;
		} else {
			return opt;
		}

	}

	@Override
	public Optional<Student> findByStudentName(String studentName) {
		Student student = new Student();
		student.setStudentName(studentName);
		boolean check1 = validateStudent(student);
		Optional<Student> opt = null;
		if (check1) {
			opt = studentDao.findByStudentName(studentName);
			return opt;
		} else {
			return opt;
		}

	}

	@Override
	public List<Student> findAll() {

		List<Student> listStudents = studentDao.findAll();
		return listStudents;
	}

	@Override
	public Optional<Student> update(Student student) {
		boolean flag = validateStudent(student);
		if (flag) {
			return studentDao.update(student);
		} else {
			return null;
		}

	}

	@Override
	public Integer deleteByStudentId(Integer studentId) {

		Student student = new Student();
		// student.setStudentId(studentId);
		Integer intNum = null;
		boolean flag = validateStudent(student);
		if (flag) {
			intNum = studentDao.deleteByStudentId(studentId);
		} else {
			intNum = null;
		}
		return intNum;
	}

	@Override
	public boolean validateStudent(Student student) {

		int length = String.valueOf(student.getStudentId()).length();
		boolean check;
		if (length >= 1 || student.getStudentName().length() >= 3) {
			check = true;
		} else {

			check = false;

		}
		return check;
	}

}
