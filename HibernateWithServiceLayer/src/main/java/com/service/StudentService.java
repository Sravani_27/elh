package com.service;

import java.util.List;
import java.util.Optional;

import com.exception.StudentException;
import com.model.Student;

public interface StudentService {

	public abstract Optional<Student> save(Student student);

	public abstract Optional<Student> findByStudentId(Integer studentId);

	public abstract Optional<Student> findByStudentName(String studentName);

	public abstract List<Student> findAll();

	public abstract Optional<Student> update(Student student);

	public abstract Integer deleteByStudentId(Integer studentId);

	public abstract boolean validateStudent(Student student);

}
