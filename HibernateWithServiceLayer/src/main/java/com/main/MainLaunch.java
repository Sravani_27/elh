package com.main;

import java.util.List;

import com.model.Student;
import com.service.StudentService;
import com.service.StudentServiceImpl;

public class MainLaunch {

	public static void main(String[] args) {

		StudentService studentService = new StudentServiceImpl();

		// Student student1 = new Student();
		// student1.setStudentName("Cucumber");
		// student1.setStudentMarks(55);
		// Optional<Student> optional = studentService.save(student1);
		//
		// if (optional.isPresent()) {
		// System.out.println(optional.get().getStudentId());
		// System.out.println(optional.get().getStudentName());
		// System.out.println(optional.get().getStudentMarks());
		// } else {
		// System.out.println("Oops Error...!");
		// }

		List<Student> list = studentService.findAll();
		if (list.isEmpty()) {
			System.out.println("No Data...!!!");
		} else {
			for (Student student : list) {
				System.out.println(student.getStudentId());
				System.out.println(student.getStudentName());
				System.out.println(student.getStudentMarks());
			}

			// Optional<Student> optStudent = studentService.findByStudentId(6);
			// Optional<Student> optStudent = studentService.findByStudentName("Carrot");
			// if (optStudent.isPresent()) {
			// Student studnet = optStudent.get();
			// System.out.println(studnet.getStudentId());
			// System.out.println(studnet.getStudentName());
			// System.out.println(studnet.getStudentMarks());
			// } else {
			// System.out.println("No Data Available...!!");
			// }

			// Student student1 = new Student();
			// student1.setStudentId(10);
			// student1.setStudentName("Olaf");
			// student1.setStudentMarks(100);
			// Optional<Student> updateStudent = studentService.update(student1);
			// if (updateStudent.isPresent()) {
			// System.out.println(updateStudent.get().getStudentName());
			// } else {
			// System.out.println("No Data Present...!!");
			// }

			// Integer numOfDeletes = studentService.deleteByStudentId(5);
			// if (numOfDeletes != null) {
			// System.out.println("Student Deleted " );
			// } else {
			// System.out.println("Oops Error");
			// }
		}

	}
}
