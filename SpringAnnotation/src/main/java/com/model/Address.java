package com.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Address {

	@Value(value = "38109")
	private int doorNumber;
	@Value(value = "Seoul")
	private String city;
	@Value(value = "South Korea")
	private String state;

	public Address() {
		super();

	}

	public Address(int doorNumber, String city, String state) {
		super();
		this.doorNumber = doorNumber;
		this.city = city;
		this.state = state;
	}

	public int getDoorNumber() {
		return doorNumber;
	}

	public void setDoorNumber(int doorNumber) {
		this.doorNumber = doorNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
