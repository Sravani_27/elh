package com.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.model.Address;

@Component(value = "alias")
// @Component
// @Service
// @Repository
public class Student {

	// @Value(value = "31124")
	private int studentNumber;
	// @Value(value = "Lee Min Ho")
	private String studentName;
	// @Value(value = "34")
	private int studentMarks;

	@Autowired
	private Address address;

	public Student() {
		super();

	}

	@Autowired
	public Student(@Value(value = "1122") int studentNumber, @Value(value = "LEE JONG SEOK") String studentName,
			@Value(value = "34") int studentMarks) {
		super();
		this.studentNumber = studentNumber;
		this.studentName = studentName;
		this.studentMarks = studentMarks;
	}

	public int getStudentNumber() {
		return studentNumber;
	}

	// @Value(value = "112")
	public void setStudentNumber(int studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	// @Value(value = "Min Ho")
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentMarks() {
		return studentMarks;
	}

	// @Value(value = "34")
	public void setStudentMarks(int studentMarks) {
		this.studentMarks = studentMarks;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
