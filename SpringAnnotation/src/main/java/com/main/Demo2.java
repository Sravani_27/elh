package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;
import com.model.Address;


public class Demo2 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/SpringConfig.xml");
		/*
		 * Student student = (Student) applicationContext.getBean("student");
		 * System.out.println("Spring Annotation + service+ Repository:");
		 */

		Student student = (Student) applicationContext.getBean("alias");
		// System.out.println("Component + value:");
		System.out.println("Using @value above on setters:");
		System.out.println(student.getStudentNumber());
		System.out.println(student.getStudentName());
		System.out.println(student.getStudentMarks());
		
		System.out.println("Address Details: ");
		System.out.println(student.getAddress().getDoorNumber());
		System.out.println(student.getAddress().getCity());
		System.out.println(student.getAddress().getState());

	}

}
