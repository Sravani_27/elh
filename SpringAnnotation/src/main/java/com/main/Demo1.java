package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.model.Student;

public class Demo1 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Demo.class);
		Student student = applicationContext.getBean(Student.class);
		student.setStudentNumber(1410492);
		student.setStudentName("Sravani");
		student.setStudentMarks(75);
		System.out.println(student.getStudentNumber());
		System.out.println(student.getStudentName());
		System.out.println(student.getStudentMarks());

		System.out.println("End of the Application");
	}

}
