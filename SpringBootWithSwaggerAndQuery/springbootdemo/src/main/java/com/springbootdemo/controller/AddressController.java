package com.springbootdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.model.Address;
import com.springbootdemo.service.AddressService;

@RestController
@RequestMapping(value = "/addressdemo")
public class AddressController {

	@Autowired
	private AddressService addressService;

	// request handler
	// @RequestMapping
	@GetMapping(value = "/")
	public String sayHello() {
		return "Welcome to Spring Boot Application";
	}

	@PostMapping(value = "createaddress")
	public ResponseEntity<Address> addAddress(@RequestBody Address address) {
		Address addressVar = null;
		if (address != null) {
			addressVar = addressService.addAddress(address);
		}
		return new ResponseEntity<Address>(addressVar, HttpStatus.CREATED);
	}

	@GetMapping(value = "readaddressbyid/{doorNumber}")
	public Address readAddress(@PathVariable("doorNumber") int doorNumber) {
		Address addressVar = null;
		if (doorNumber > 0) {
			addressVar = addressService.readAddressByDoorNumber(doorNumber);
		}
		return addressVar;
	}

	@GetMapping(value = "readaddressbycity/{city}")
	public Address readAddressByCity(@PathVariable("city") String city) {
		Address addressVar = null;
		if (city != null) {
			addressVar = addressService.readAddressByCity(city);
		}
		return addressVar;
	}

	@GetMapping(value = "readaddressbystate/{state}")
	public Address readAddressByState(@PathVariable("state") String state) {
		Address addressVar = null;
		if (state != null) {
			addressVar = addressService.readAddressByState(state);
		}
		return addressVar;
	}

	@PutMapping(value = "/upadateaddress")
	public Address updateAddress(@RequestBody Address address) {
		return addressService.alterAddress(address);
	}

	@DeleteMapping(value = "/deleteaddress/{doorNumber}")
	public int deleteAddress(@PathVariable("doorNumber") int doorNumber) {
		return addressService.deleteAddress(doorNumber);
	}

	@GetMapping("/Query/{doorNo}")
	public ResponseEntity<String> customQuery(@PathVariable("doorNo") int doorNumber) {
		String city = addressService.getCityByDoorNumber(doorNumber);
		return new ResponseEntity<String>(city, HttpStatus.FOUND);
	}

	@GetMapping("/getlistofaddresses")
	public ResponseEntity<List<Address>> nativeQuery() {
		return new ResponseEntity<List<Address>>(addressService.nativeQuery(), HttpStatus.OK);
	}

}
