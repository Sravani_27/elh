package com.springbootdemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springbootdemo.model.Address;
import com.springbootdemo.repository.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressRepository addressRepository;

	@Override
	@Transactional
	public Address addAddress(Address address) {
		return addressRepository.save(address);

	}

	@Override
	@Transactional
	public Address readAddressByCity(String city) {
		Address addressVar = null;
		Optional<Address> optional = addressRepository.findAddressByCity(city);
		if (optional.isPresent()) {
			addressVar = optional.get();
		}
		return addressVar;
	}

	@Override
	@Transactional
	public Address readAddressByState(String state) {

		Address addressVar = null;
		Optional<Address> optional = addressRepository.findAddressByState(state);
		if (optional.isPresent()) {
			addressVar = optional.get();
		}
		return addressVar;
	}

	@Override
	@Transactional
	public Address readAddressByDoorNumber(int doorNumber) {
		Address address = null;
		Optional<Address> optional = addressRepository.findById(doorNumber);
		if (optional.isPresent()) {
			address = optional.get();
		}
		return address;
	}

	@Override
	@Transactional
	public Address alterAddress(Address address) {

		return addressRepository.save(address);
	}

	@Override
	@Transactional
	public int deleteAddress(int doorNumber) {

		addressRepository.deleteById(doorNumber);
		return doorNumber;
	}

	@Override
	public String getCityByDoorNumber(int doorNumber) {
		String city = addressRepository.findCityByDoorNumber(doorNumber);
		return city;
	}

	@Override
	public List<Address> nativeQuery() {
		
		return addressRepository.getAllAddresses();
	}

}
