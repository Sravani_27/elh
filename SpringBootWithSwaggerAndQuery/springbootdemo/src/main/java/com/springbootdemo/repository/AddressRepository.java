package com.springbootdemo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.springbootdemo.model.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {

	public Optional<Address> findAddressByCity(String city);

	public Optional<Address> findAddressByState(String state);

	@Query("SELECT a.city FROM Address as a where a.doorNumber = :id") // ORM
	public abstract String findCityByDoorNumber(@Param("id") Integer doorNumber); // custom query
	
	@Query(value = "select * from address_table", nativeQuery = true) //not ORM -SQL Query
		public abstract List<Address> getAllAddresses();
		
	
}
