package com.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Service {

	private static final Logger LOGGER = LogManager.getLogger(Service.class);

	public int add(int num1, int num2) {

		LOGGER.info("Display values of num1 and num2" + num1 + " " + num2);
		return num1 + num2;

	}

}
