package com.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.Service;

public class LoggerMain {

	private static final Logger LOGGER = LogManager.getLogger(LoggerMain.class);

	public static void main(String[] args) {

		LOGGER.info("You are in main method");

		Service service = new Service();
		int result = service.add(12, 4);
		System.out.println("Adding two numbers: " + result);
		LOGGER.info("Data received :" + result);
		System.out.println("End of the main");

	}

}
