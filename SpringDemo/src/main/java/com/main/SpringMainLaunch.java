package com.main;

import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Address;
import com.model.Employee;

public class SpringMainLaunch {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/configue/SpringContext.xml");// Location
																														// of
		Employee employee = (Employee) applicationContext.getBean("employeeId"); // the

		/*
		 * System.out.println("Spring: " + employee); employee.setEmployeeId(111);
		 * employee.setEmployeeName("Min ho"); employee.setEmployeeSalary(200000f);
		 */

		System.out.println(employee.getEmployeeId());
		System.out.println(employee.getEmployeeName());
		System.out.println(employee.getEmployeeSalary());

		System.out.println("Address Details:");
		Set<Address> addresses = employee.getAddresses();

		for (Address addre : addresses) {
			System.out.println(addre.getDoorNumber());
			System.out.println(addre.getCity());
			System.out.println(addre.getState());
		}

		/*
		 * System.out.println(employee.getAddress().getDoorNumber());
		 * System.out.println(employee.getAddress().getCity());
		 * System.out.println(employee.getAddress().getState());
		 */

		((ConfigurableApplicationContext) applicationContext).registerShutdownHook();

		System.out.println("End of the main method");
	}

}
