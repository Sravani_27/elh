package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Address;

public class Spring2 {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/configue/SpringContext.xml");
		Address address = (Address) applicationContext.getBean("address");
		address.setDoorNumber(3244);
		address.setCity("Seoul");
		address.setState("South Korea");

		System.out.println("Address Details: ");
		System.out.println("Door number: " + address.getDoorNumber());
		System.out.println("City: " + address.getCity());
		System.out.println("State: " + address.getState());
	}

}
