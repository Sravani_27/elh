package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Address;

public class SpringMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/configue/SpringContext.xml");
		Address address = (Address) applicationContext.getBean("address");
		System.out.println(address);
		address.setDoorNumber(1122);
		address.setCity("Busan");
		address.setState("South Korea");

		Address address1 = (Address) applicationContext.getBean("address");
		System.out.println(address1);
		System.out.println("address1 : " + address1.getDoorNumber());
		System.out.println("address1 : " + address1.getCity());
		System.out.println("address1 : " + address1.getState());

	}

}
