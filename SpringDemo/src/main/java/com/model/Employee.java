package com.model;

import java.util.Set;

/*import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;*/

public class Employee /* implements InitializingBean, DisposableBean */ {

	private int employeeId;
	private String employeeName;
	private float employeeSalary;

	// private Address address;
	private Set<Address> addresses;

	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	public void hello() {
		System.out.println("this is init method");
	}

	public void bye() {
		System.out.println("this is destroy method");
	}

	public Employee() {
		super();
		System.out.println("Inside Employee file ");

	}

	public Employee(int employeeId, String employeeName, float employeeSalary) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(float employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	/*
	 * public Address getAddress() { return address; }
	 * 
	 * public void setAddress(Address address) { this.address = address; }
	 */

	/*
	 * @Override public void afterPropertiesSet() throws Exception {
	 * System.out.println("Inside after property set method");//when bean is created
	 * 
	 * }
	 * 
	 * @Override public void destroy() throws Exception {
	 * System.out.println("Inside destroy method");
	 * 
	 * }
	 */

}
