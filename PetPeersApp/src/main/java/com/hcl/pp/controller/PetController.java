package com.hcl.pp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;
import com.hcl.pp.validator.PetValidator;

@RestController
public class PetController {

	@Autowired
	private PetService petService;

	@GetMapping("/pets/petDetail")
	public ResponseEntity<List<Pet>> petDetails() {
		List<Pet> pet = petService.getAllPets();
		return new ResponseEntity<List<Pet>>(pet, HttpStatus.FOUND);
	}

	@PostMapping("/pets/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody PetValidator pet) {
		ResponseEntity<Pet> responseEntity = null;
		if (pet != null) {
			responseEntity = new ResponseEntity<Pet>(petService.savePet(pet), HttpStatus.CREATED);
		} else {
			responseEntity = new ResponseEntity<Pet>(petService.savePet(pet), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@GetMapping("/pets")
	public List<Pet> petHome() {

		return petService.getAllPets();
	}

}
