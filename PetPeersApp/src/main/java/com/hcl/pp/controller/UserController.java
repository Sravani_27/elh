package com.hcl.pp.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.InvalidLoginException;
import com.hcl.pp.exception.NoPetAvailableException;
import com.hcl.pp.exception.PetSoledOutFoundException;
import com.hcl.pp.exception.UserNameAndPasswordSame;
import com.hcl.pp.exception.UserNotFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/user/add")
	public ResponseEntity<User> addUser(@RequestBody @Valid User user) throws UserNameAndPasswordSame {
		User user1 = null;
		try {
			if (user != null) {

				ModelMapper modelMapper = new ModelMapper();
				UserValidator userRequest = modelMapper.map(user, UserValidator.class);

				if (userRequest.getUsername().equalsIgnoreCase(userRequest.getUserPassword())) {

					throw new UserNameAndPasswordSame("username and password should not be same");
				} else {
					if (userRequest.getUserPassword().equals(userRequest.getConfirmPassword())) {
						user1 = userService.addUser(user);
						return new ResponseEntity<User>(user1, HttpStatus.CREATED);
					} else {

						throw new UserNotFoundException("Password do not match");
					}
				}
			} else {
				throw new UserNotFoundException("please enter");
			}
		} catch (UserNameAndPasswordSame e) {
			System.err.println(e);
		}
		return new ResponseEntity<User>(user1, HttpStatus.CREATED);
	}

	@GetMapping("/user/login")
	public ResponseEntity<User> loginUser(@Valid @RequestParam("name") String name,
			@RequestParam("password") String password, @RequestParam("confirmPassword") String confirmPassword) {

		User user = userService.authenticateUser(name, password);

		ModelMapper modelMapper = new ModelMapper();
		LoginValidator user1 = modelMapper.map(user, LoginValidator.class);

		if (user1.getUsername().equals(user1.getUserPassword())) {

			throw new InvalidLoginException("username and password must not be same");

		} else {
			if (user1.getUserPassword().equals(user1.getConfirmPassword())) {
				user = userService.authenticateUser(name, password);
			} else {

				throw new InvalidLoginException("password and confirm password should be same");
			}
		}
		return new ResponseEntity<User>(user, HttpStatus.FOUND);

	}

	@GetMapping("/user/logout")
	public ResponseEntity<String> logout() {
		return new ResponseEntity<String>("Please login!!", HttpStatus.OK);
	}

	@GetMapping("/pets/myPets")
	public ResponseEntity<Pet> myPets(@RequestParam("userId") int userId) throws NoPetAvailableException {
		Pet pet = userService.getMyPets(userId);
		return new ResponseEntity<Pet>(pet, HttpStatus.FOUND);
	}

	@PutMapping("/pets/buyPet/{petId}")
	public Pet buyPet(@PathVariable int petId, @RequestParam int userId) throws PetSoledOutFoundException {
		Pet pet = null;

		pet = userService.buyPet(petId, userId);

		return pet;

	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, String> dataIntegrityViolationException(HttpServletRequest req,
			DataIntegrityViolationException ex) {
		Map<String, String> errors = new HashMap<>();
		errors.put("message", "User Name is already use. please select different User Name");
		errors.put("path", req.getContextPath().toString());
		return errors;
	}

}
