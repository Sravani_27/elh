package com.hcl.pp.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.validator.PetValidator;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Override
	@Transactional
	public Pet savePet(PetValidator pet) {

		Pet newPet = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequest = modelMapper.map(pet, Pet.class);
		if (petRequest != null) {
			newPet = petRepository.save(petRequest);
		}
		return newPet;
	}

	@Override
	@Transactional
	public List<Pet> getAllPets() {

		return petRepository.findAll();
	}

	@Override
	@Transactional
	public Pet getPetById(int id) {

		return petRepository.getPetById(id);
	}

	@Override
	public List<Pet> fetchAll() {

		return petRepository.findAll();
	}

}
