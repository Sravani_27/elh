package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.model.Pet;
import com.hcl.pp.validator.PetValidator;

public interface PetService {

	public abstract Pet savePet(PetValidator pet);

	public abstract List<Pet> getAllPets();

	public abstract Pet getPetById(int id);

	public abstract List<Pet> fetchAll();

}
