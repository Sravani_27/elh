package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.exception.NoPetAvailableException;
import com.hcl.pp.exception.PetSoledOutFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

public interface UserService {

	public abstract User addUser(User user);

	public abstract User updateUser(User user);

	public abstract List<User> listUsers();

	public abstract User getUserById(int id);

	public abstract User findByUserName(String username);

	public abstract User removeUser(int id);

	public abstract Pet buyPet(int id, int userId) throws PetSoledOutFoundException;

	public abstract Pet getMyPets(int userId) throws NoPetAvailableException;

	public abstract User authenticateUser(String name, String password);

}
