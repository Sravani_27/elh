package com.hcl.pp.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.NoPetAvailableException;
import com.hcl.pp.exception.PetSoledOutFoundException;
import com.hcl.pp.exception.RecordNotFoundException;
import com.hcl.pp.exception.UserNotFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetRepository petRepository;

	@Override
	@Transactional
	public User addUser(User user1) {

		User user = new User();
		user.setUsername(user1.getUsername());
		user.setUserPassword(user1.getUserPassword());
		user.setConfirmPassword(user1.getConfirmPassword());
		Set<Pet> listPet = new HashSet<>();
		for (Pet pet : user1.getPets()) {
			Pet pp = new Pet();
			pp.setName(pet.getName());
			pp.setAge(pet.getAge());
			pp.setPlace(pet.getPlace());
			pp.setOwner(user);
			listPet.add(pp);
		}
		user.setPets(listPet);

		User adduser = userRepository.save(user);
		return adduser;

	}

	@Override
	@Transactional
	public User updateUser(User user) {
		Optional<User> optionalUser = userRepository.findById(user.getId());
		User user1 = null;
		if (optionalUser.isPresent()) {
			user1 = optionalUser.get();
			return userRepository.save(user1);

		} else {
			throw new RecordNotFoundException();
		}

	}

	@Override
	@Transactional
	public List<User> listUsers() {

		return userRepository.findAll();
	}

	@Override
	@Transactional
	public User getUserById(int id) {
		User user = null;
		Optional<User> optional = userRepository.findById(id);
		if (optional.isPresent()) {
			user = optional.get();
		}
		return user;

	}

	@Override
	@Transactional
	public User findByUserName(String username) {
		return userRepository.findByUsername(username);

	}

	@Override
	@Transactional
	public User removeUser(int id) {
		User user = getUserById(id);
		userRepository.deleteById(id);
		return user;
	}

	@Override
	@Transactional
	public Pet buyPet(int id, int userId) throws PetSoledOutFoundException {
		Pet petBought = null;
		Optional<User> userExists = userRepository.findById(userId);
		if (userExists != null) {
			Pet petExists = petRepository.getPetById(id);
			if (petExists.getOwner() != null) {
				throw new PetSoledOutFoundException("Pet is already taken Please try another!");
			} else {

				petBought = petExists;
			}

		} else {
			throw new PetSoledOutFoundException("User not exist Please Register !");
		}

		return petBought;
	}

	@Override
	@Transactional
	public Pet getMyPets(int userId) throws NoPetAvailableException {
		Pet myPets = null;
		boolean isUserValid = false;
		for (User u : userRepository.findAll()) {
			if (u.getId() == userId) {
				isUserValid = true;
			}
		}

		if (isUserValid) {
			for (Pet p : petRepository.getAllRecords()) {
				if (p.getOwner().getId() == userId) {
					myPets = p;
					return myPets;
				} else {
					throw new NoPetAvailableException("You don't have pets");
				}
			}
		} else {

			throw new UserNotFoundException("User not Exist Please Try again!");

		}

		return myPets;

	}

	@Override
	@Transactional
	public User authenticateUser(String name, String password) {
		User user1 = null;
		for (User u : userRepository.findAll()) {
			if (u.getUsername().equals(name) && u.getUserPassword().equals(password)) {

				user1 = u;

			}
		}

		return user1;
	}

}
