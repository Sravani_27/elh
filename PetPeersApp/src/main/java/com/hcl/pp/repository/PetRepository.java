package com.hcl.pp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Integer> {

	@Query("SELECT p FROM Pet as p where p.id = :id")
	public abstract Pet getPetById(@Param("id") int id);

	@Query(value = "select * from pets", nativeQuery = true)
	public abstract List<Pet> getAllRecords();

}
