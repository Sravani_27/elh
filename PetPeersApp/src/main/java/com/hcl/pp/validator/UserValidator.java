package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserValidator implements Serializable {

	private static final long serialVersionUID = -8689439148730471833L;

	@NotEmpty(message = "username can't be empty")
	@NotNull(message = "username can't be null")
	private String username;
	@NotEmpty(message = "Password can't be empty")
	@NotNull(message = "Password can't be null")
	private String userPassword;
	@NotEmpty(message = "confirmPassword can't be empty")
	@NotNull(message = "confirmPassword can't be null")
	private String confirmPassword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

}
