package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PetValidator implements Serializable {

	private static final long serialVersionUID = 6069812616566836899L;

	@NotNull(message = "Pet age can't be null")
	@Digits(integer = 2, fraction = 0, message = "Age must be in number")
	@Min(value = 0, message = "Age must be minimum 0years")
	@Max(value = 99, message = "Age must be maximum upto 99 years")
	private int age;

	@NotNull(message = "Pet Name can't be null")
	@NotEmpty(message = "Pet Name can't be empty")
	private String name;

	@NotNull(message = "Place can't be null")
	@NotEmpty(message = "Place can't be empty")
	private String place;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

}
