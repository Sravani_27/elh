package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pets")
public class Pet implements Serializable {

	private static final long serialVersionUID = 5634824914007820531L;

	@Id
	@Column(nullable = false, length = 2)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "pet_name", nullable = false, length = 55)
	private String name;

	@Column(name = "pet_age", length = 2)
	private int age;

	@Column(name = "pet_place", length = 55)
	private String place;

	@JoinColumn(name = "pet_ownerid")
	@ManyToOne(fetch = FetchType.EAGER)
	private User owner;

	public Pet() {
		super();
	}

	public Pet(int id, String name, int age, String place, User owner) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.place = place;
		this.owner = owner;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

}
