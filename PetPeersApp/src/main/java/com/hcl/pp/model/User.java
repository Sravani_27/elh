package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pet_user")
public class User implements Serializable {

	private static final long serialVersionUID = -8792266564533008044L;

	@Id
	@Column(nullable = false, length = 2)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "user_name", nullable = false, unique = true, length = 55)
	private String username;

	@Column(name = "user_passwd", nullable = false, length = 55)
	private String userPassword;

	@Transient
	private String confirmPassword;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({ "owner" })
	private Set<Pet> pets;

	public User() {
		super();
	}

	public User(int id, String username, String userPassword, String confirmPassword, Set<Pet> pets) {
		super();
		this.id = id;
		this.username = username;
		this.userPassword = userPassword;
		this.confirmPassword = confirmPassword;
		this.pets = pets;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Set<Pet> getPets() {
		return pets;
	}

	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}
}
