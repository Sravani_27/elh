package com.hcl.pp.exception;

public class InvalidLoginException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2404699929944611476L;

	private String message;

	public InvalidLoginException() {
		super();
	}

	public InvalidLoginException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
