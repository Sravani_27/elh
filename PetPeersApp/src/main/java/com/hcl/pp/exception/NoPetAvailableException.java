package com.hcl.pp.exception;

public class NoPetAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5880069472109044673L;

	private String message;

	public NoPetAvailableException() {
		super();
	}

	public NoPetAvailableException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
