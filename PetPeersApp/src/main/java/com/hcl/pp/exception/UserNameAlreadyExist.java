package com.hcl.pp.exception;

public class UserNameAlreadyExist extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5628538664265434368L;

	private String code;
	private String message;

	public UserNameAlreadyExist() {
		super();
	}

	public UserNameAlreadyExist(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
