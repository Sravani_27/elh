package com.hcl.pp.exception;

public class PetSoledOutFoundException extends Exception {

	private static final long serialVersionUID = 3686456425058996159L;

	private String message;

	public PetSoledOutFoundException() {
		super();
	}

	public PetSoledOutFoundException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
