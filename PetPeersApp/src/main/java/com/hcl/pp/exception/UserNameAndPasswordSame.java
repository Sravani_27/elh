package com.hcl.pp.exception;

public class UserNameAndPasswordSame extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1069235389832409597L;

	private String code;
	private String message;

	public UserNameAndPasswordSame() {
		super();
	}

	public UserNameAndPasswordSame(String message) {
		super(message);
		this.message = message;
	}

	public UserNameAndPasswordSame(String code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
