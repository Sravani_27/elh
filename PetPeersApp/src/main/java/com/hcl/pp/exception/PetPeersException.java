package com.hcl.pp.exception;

public class PetPeersException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6531625734425036733L;

	private String message;

	public PetPeersException() {
		super();
	}

	public PetPeersException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
