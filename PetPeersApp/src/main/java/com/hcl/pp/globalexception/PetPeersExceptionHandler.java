package com.hcl.pp.globalexception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.hcl.pp.exception.InvalidLoginException;
import com.hcl.pp.exception.NoPetAvailableException;
import com.hcl.pp.exception.PetSoledOutFoundException;
import com.hcl.pp.exception.UserNameAlreadyExist;
import com.hcl.pp.exception.UserNameAndPasswordSame;
import com.hcl.pp.exception.UserNotFoundException;

@ControllerAdvice
public class PetPeersExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(UserNameAlreadyExist.class)
	public ResponseEntity<String> handleUserNameAlreadyExist(UserNameAlreadyExist alreadyExist) {
		return new ResponseEntity<String>("User Name already in use. Please select a different User Name",
				HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(UserNameAndPasswordSame.class)
	public ResponseEntity<String> handleUserNameAndPasswordSame(UserNameAndPasswordSame nameAndPasswordSame) {
		return new ResponseEntity<String>(nameAndPasswordSame.getMessage(), HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException notFoundException) {
		return new ResponseEntity<String>(notFoundException.getLocalizedMessage(), HttpStatus.CREATED);

	}

	@ExceptionHandler(InvalidLoginException.class)
	public ResponseEntity<String> handleInvalidLoginException(InvalidLoginException exception) {
		return new ResponseEntity<String>(exception.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PetSoledOutFoundException.class)
	public ResponseEntity<String> handlePetSoledOutFoundException(PetSoledOutFoundException soledOutFoundException) {
		return new ResponseEntity<String>(soledOutFoundException.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NoPetAvailableException.class)
	public ResponseEntity<String> handleNoPetAvailableException(NoPetAvailableException noPetAvailableException) {
		return new ResponseEntity<String>(noPetAvailableException.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
	}
}
