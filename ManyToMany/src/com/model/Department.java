package com.model;

public class Department {

	private String departmentName;
	private Student[] students;
	
	public Department() {
		super();
		
	}

	public Department(String departmentName, Student[] students) {
		super();
		this.departmentName = departmentName;
		this.students = students;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Student[] getStudents() {
		return students;
	}

	public void setStudents(Student[] students) {
		this.students = students;
	}
	
	
}
