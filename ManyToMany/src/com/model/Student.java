package com.model;

public class Student {

	private String studentName;
	private int studentId;
	private String studentDept;

	public Student() {
		super();

	}

	public Student(String studentName, int studentId, String studentDept) {
		super();
		this.studentName = studentName;
		this.studentId = studentId;
		this.studentDept = studentDept;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentDept() {
		return studentDept;
	}

	public void setStudentDept(String studentDept) {
		this.studentDept = studentDept;
	}

}
