package com.model;

public class Library {

	private Book[] books;

	public Library() {
		super();
		
	}

	public Library(Book[] books) {
		super();
		this.books = books;
	}

	public Book[] getBooks() {
		return books;
	}

	public void setBooks(Book[] books) {
		this.books = books;
	}
	
	
	
}
