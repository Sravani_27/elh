package com.model;

public class Institute {

	private String instituteName;
	private Department[] departments;

	public Institute() {
		super();

	}

	public Institute(String instituteName, Department[] departments) {
		super();
		this.instituteName = instituteName;
		this.departments = departments;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public Department[] getDepartments() {
		return departments;
	}

	public void setDepartments(Department[] departments) {
		this.departments = departments;
	}

}
