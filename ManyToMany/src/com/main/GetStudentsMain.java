package com.main;

import com.model.Department;
import com.model.Institute;
import com.model.Student;

public class GetStudentsMain {

	public static void main(String[] args) {

		Student student1 = new Student("Sravani", 123456, "ECE");
		Student student2 = new Student("Ravali", 76890, "ECE");
		Student student3 = new Student("Navya", 37812, "CSE");
		Student student4 = new Student("Sree", 11111, "MBA");

		Student[] students = new Student[4];
		students[0] = student1;
		students[1] = student2;
		students[2] = student3;
		students[3] = student4;

		Department department1 = new Department("ECE", students);
		Department department2 = new Department("Civil", students);
		Department department3 = new Department("MBA", students);
		Department department4 = new Department("MCA", students);

		Department[] departments = new Department[4];
		departments[0] = department1;
		departments[1] = department2;
		departments[2] = department3;
		departments[3] = department4;

		//Institute institute = new Institute("AITS", departments);

		for (int i = 0; i < departments.length; i++) {
			for (int j = 0; j < students.length; j++) {

				if (departments[i].getDepartmentName().equals(students[j].getStudentDept())) {
					System.out.println("Student Details in the each department:" + students[j].getStudentId() + " "
							+ students[j].getStudentName() + " " + students[j].getStudentDept());

				}
			}

		}

	}

}
