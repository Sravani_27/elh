package com.main;

import com.model.Book;

public class LibraryMain {

	public static void main(String[] args) {
		
		Book book1 = new Book("Effective Java", "Joshua Bloch");
		Book book2 = new Book("Thinking in Java", "Bruce Eckel");

		Book[] books = new Book[2];
		books[0] = book1;
		books[1] = book2;
		
		System.out.println("Title of the book: " + books[0].getTitle());
		System.out.println("Author of the book: " + books[0].getAuthor());
		
		System.out.println("Title of the book: " + books[1].getTitle());
		System.out.println("Author of the book: " + books[1].getAuthor());
	}

}
