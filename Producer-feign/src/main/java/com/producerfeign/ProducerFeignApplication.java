package com.producerfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ProducerFeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerFeignApplication.class, args);
	}

}
