package com.producerservices.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.producerservices.model.Employee;

@RestController
@RequestMapping(value = "producer")
public class EmployeeProducer {

	@GetMapping("/world")
	public ResponseEntity<String> sayWorld() {
		return new ResponseEntity<String>("Hello There!", HttpStatus.OK);
	}

	@PostMapping("/addemp")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee emp) {
		System.out.println(emp.getEmpNo());
		System.out.println(emp.getEmpName());
		System.out.println(emp.getSalary());
		System.out.println(emp.getDeptId());

		return new ResponseEntity<Employee>(emp, HttpStatus.CREATED);
	}

	@GetMapping("/readall")
	public ResponseEntity<List<Employee>> readAllEmployee() {
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(11, "banana", 1111f, 1));
		employees.add(new Employee(12, "carry", 1231f, 2));
		employees.add(new Employee(22, "moon", 1931f, 1));
		employees.add(new Employee(33, "sun", 2231f, 2));
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/readempbydept/{deptId}")
	public ResponseEntity<List<Employee>>  readEmployeeByDeptId(@PathVariable int deptId){
		return null;
		
	}
}
