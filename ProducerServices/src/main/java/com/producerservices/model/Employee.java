package com.producerservices.model;

public class Employee {

	private int empNo;
	private String empName;
	private float salary;
	private int deptId;

	public Employee() {
		super();
	}

	public Employee(int empNo, String empName, float salary, int deptId) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.deptId = deptId;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
