package com.model;

public class Calculator {

	public int div(int num1, int num2) throws ArithmeticException {
		int ans = 0;
		if (num1 > 0 && num2 > 0) {
			ans = num1 / num2;

		} else {
			throw new ArithmeticException();

		}
		return ans;
	}

}
