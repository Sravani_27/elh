package com.main;

import com.model.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		try {
			int result = calculator.div(23, 6);

			if (result == 0) {
				System.out.println("Something wrong");
			} else {
				System.out.println("Division of 2 numbers: " + result);
			}
		} catch (ArithmeticException ae) {
			System.err.println(ae.getMessage());
		} finally {
			System.out.println("Inside the final block");
		}
		System.out.println("End of the programm");
	}

}
