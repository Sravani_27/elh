package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FirstDemo {

	@RequestMapping(value = "abc")
	public String functionOne() {
		return "Success";//view name
	}

	@RequestMapping(value = "efg")
	public String functionTwo() {
		return "thankyou";
	}

}
