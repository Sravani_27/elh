package com.springbatchdemo.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import com.springbatchdemo.model.User;

@Configuration
//@EnableBatchProcessing
public class SpringBatchConfig {

	@Bean
	public Job job(JobBuilderFactory jbf, StepBuilderFactory sbf, ItemReader<User> itemReader,
			ItemProcessor<User, User> itemProcessor, ItemWriter<User> itemWriter) {
		Step step1 = sbf.get("CSV-to-Database").<User, User>chunk(100).reader(itemReader).processor(itemProcessor)
				.writer(itemWriter).build();

		Step step2 = sbf.get("Delete File").<User, User>chunk(100).reader(itemReader).processor(itemProcessor)
				.writer(itemWriter).build();

		Job job = jbf.get("Job1").incrementer(new RunIdIncrementer()).start(step1).next(step2).build();

		return job;
	}

	@Bean
	public FlatFileItemReader<User> itemReader() {

		FlatFileItemReader<User> fileItemReader = new FlatFileItemReader<User>();
		fileItemReader.setResource(new FileSystemResource("src/main/resources/users.csv"));
		fileItemReader.setName("CSV-Reader");
		fileItemReader.setLinesToSkip(1);
		fileItemReader.setLineMapper(lineMapper());
		return fileItemReader;

	}

	@Bean
	public LineMapper<User> lineMapper() {

		DefaultLineMapper<User> defaultLineMapper = new DefaultLineMapper<User>();
		DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
		delimitedLineTokenizer.setDelimiter(",");
		delimitedLineTokenizer.setStrict(false);
		delimitedLineTokenizer.setNames("id", "name", "dept", "salary");

		BeanWrapperFieldSetMapper<User> fieldSetMapper = new BeanWrapperFieldSetMapper<User>();
		fieldSetMapper.setTargetType(User.class);
		defaultLineMapper.setLineTokenizer(delimitedLineTokenizer);
		defaultLineMapper.setFieldSetMapper(fieldSetMapper);

		return defaultLineMapper;

	}
}
