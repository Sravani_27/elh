package com.springbatchdemo.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springbatchdemo.model.User;
import com.springbatchdemo.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DBWriter implements ItemWriter<User> {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	public DBWriter(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public void write(List<? extends User> users) throws Exception {
		log.info("Data Saved for Users: " + users);
		userRepository.saveAll(users);

	}

}
