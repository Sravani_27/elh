package com.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;

//import org.junit.internal.runners.statements.*;

import org.junit.Test;

public class CalculatorTest {

	Calculator calculator = null;

	@Before
	public void startTest() {
		calculator = new Calculator();
		System.out.println("Start of the test");
	}

	@Test
	public void testAdd() {

		assertEquals(6, calculator.add(4, 2));
	}

	@Test
	public void testAdd1() {

		assertEquals(21, calculator.add(10, 11));
	}

	@After
	public void stopTest() {
		calculator = null;
		System.out.println("End of the test");
	}

}
