package com.model;

public class Employee {
	private int empAccNo;
	private int empId;
	private String empName;
	private Bank bank;
	
	public Employee() {
		super();
		
	}

	public Employee(int empAccNo, int empId, String empName, Bank bank) {
		super();
		this.empAccNo = empAccNo;
		this.empId = empId;
		this.empName = empName;
		this.bank = bank;
	}


	public Employee(int empAccNo, int empId, String empName) {
		super();
		this.empAccNo = empAccNo;
		this.empId = empId;
		this.empName = empName;
	}

	public int getEmpAccNo() {
		return empAccNo;
	}

	public void setEmpAccNo(int empAccNo) {
		this.empAccNo = empAccNo;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	
}
