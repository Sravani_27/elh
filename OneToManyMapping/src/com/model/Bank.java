package com.model;

public class Bank {
	private String bankName;
	private String bankIfscNo;
	private Employee employee;
	
	public Bank() {
		super();
		
	}

	public Bank(String bankName, String bankIfscNo, Employee employee) {
		super();
		this.bankName = bankName;
		this.bankIfscNo = bankIfscNo;
		this.employee = employee;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankIfscNo() {
		return bankIfscNo;
	}

	public void setBankIfscNo(String bankIfscNo) {
		this.bankIfscNo = bankIfscNo;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	

}
