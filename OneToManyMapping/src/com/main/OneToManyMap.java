package com.main;

import com.model.Bank;
import com.model.Employee;

public class OneToManyMap {

	public static void main(String[] args) {
	
		Employee employee1 = new Employee(12345, 223, "cow");
		Employee employee2 = new Employee(56780, 0257, "parrot");
		
		Bank bank1 = new Bank("KTX", "KTS00987", employee1);
		Bank bank2 = new Bank("xyz", "abc000001", employee2);
	
		Bank[] banks = new Bank[2];
		banks[0] = bank1;
		banks[0] = bank2;
		
		Employee[] employees = new Employee[2];
		employees[0] = employee1;
		employees[1] = employee2;
		
		System.out.println("Display bank details of employee1:");
		System.out.println(employees[0].getEmpName());
		System.out.println(employees[0].getEmpId());
		System.out.println(employees[0].getEmpAccNo());
		employees[0].setBank(bank1);
		System.out.println(employees[0].getBank().getBankName());
		System.out.println(employees[0].getBank().getBankIfscNo());
		
		System.out.println("Display bank details of employee2:");
		System.out.println(employees[1].getEmpName());
		System.out.println(employees[1].getEmpId());
		System.out.println(employees[1].getEmpAccNo());
		employees[1].setBank(bank2);
		System.out.println(employees[1].getBank().getBankName());
		System.out.println(employees[1].getBank().getBankIfscNo());

	}

}
