package com.main;

import java.util.Scanner;

import com.service.Check;
import com.service.CheckPage;

public class LoginPage {

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter userId/SAPID: ");
		int id = scanner.nextInt();
		System.out.println("Enter your password: ");
		String password = scanner.next();

		Check check = new CheckPage();

		String messageName = check.validateUserAndPassword(id, password);

		if (messageName.equals("Inavalid username or password")) {
			System.out.println(messageName);
		} else {

			System.out.println("Welcome " + messageName);
		}

		scanner = null;
		check = null;

	}

}
