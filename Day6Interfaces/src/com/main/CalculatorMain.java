package com.main;

import com.model.Calculator;
import com.model.MyCalculator1;

public class CalculatorMain {

	public static void main(String[] args) {

		Calculator calculator  = new MyCalculator1();

		System.out.println("Adding numbers: " + calculator.add(25, 5));
		System.out.println("Substraction :" + calculator.sub(30, 11));
		System.out.println("Multiplication: " + calculator.mul(23, 6));
		System.out.println("Divison: " + calculator.div(25, 5));

		calculator = null;

	}

}
