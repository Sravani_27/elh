package com.model;

public class User {

	private int userId;
	private String passWord;
	//private String userName;

	public User() {
		super();

	}

	public User(int userId, String passWord) {
		super();
		this.userId = userId;
		this.passWord = passWord;
	}

/*	public User(int userId, String passWord, String userName) {
		super();
		this.userId = userId;
		this.passWord = passWord;
		this.userName = userName;
	}
*/
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

/*	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
*/
}
