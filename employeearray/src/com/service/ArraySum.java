package com.service;

import com.model.Employee;

public class ArraySum {

	public float totalSalary(Employee[] employees) {

		float budget = 0;
		
		for (int i = 0; i < employees.length; i++) {
			
			budget += employees[i].getEmployeeSalary();
		}
		return budget;
	}

}