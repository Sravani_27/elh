/**    
 * @author yerramreddy.sravani
 */

package com.main;

import com.model.Employee;
import com.service.EmployeeLogic;
import com.service.Search;
import com.service.ArraySum;

public class EmployeeMain {

	public static void main(String[] args) {

		Employee employee1 = new Employee(997, "tom", 3000f, "ch street Bejing ");
		Employee employee2 = new Employee(554, "mickey", 7650f, "club house Disney");

		// Displaying the data of employees
		System.out.println("Employee number: " + employee1.getEmployeeId());
		System.out.println("Employee name: " + employee1.getEmployeeName());
		System.out.println("Employee salary: " + employee1.getEmployeeSalary());
		System.out.println("Employee address: " + employee1.getEmployeeAddress());

		System.out.println("Employee number: " + employee2.getEmployeeId());
		System.out.println("Employee name: " + employee2.getEmployeeName());
		System.out.println("Employee salary: " + employee2.getEmployeeSalary());
		System.out.println("Employee address: " + employee2.getEmployeeAddress());

		EmployeeLogic employeeLogic = new EmployeeLogic();

		// comparing salary of two employees
		Employee emp = employeeLogic.compareSalary(employee1, employee2);

		System.out.println(" employee id: " + emp.getEmployeeId());
		System.out.println(" employee name: " + emp.getEmployeeName());
		System.out.println(" employee salary: " + emp.getEmployeeSalary());
		System.out.println(" employee address: " + emp.getEmployeeAddress());

		// total budget spent on employees
		Employee employee3 = new Employee(967, "shinchan", 3098f, "itaewon seoul");

		Employee[] employees = new Employee[3];

		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;

		float totalMoney = 0;

		ArraySum arraySum = new ArraySum();

		totalMoney = arraySum.totalSalary(employees);

		System.out.println("the total money Spent:" + totalMoney);

		// search for particular employee name

		Search search = new Search();

		System.out.println("Matched name: " + search.nameSearch(employees));

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employees = null;

		arraySum = null;
		search = null;

		/*
		 * Scanner scanner = new Scanner(System.in);
		 * 
		 * System.out.println("enter the employee Name: ");
		 * 
		 * String searchName = scanner.nextLine();
		 */

	}

}
