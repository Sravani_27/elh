package com.main;

public class ArrayMain {

	public static void main(String[] args) {
		
		int[] intArray = new int[6];

		for (int i = 0; i < intArray.length; i++) {

			intArray[i] = i;

		}

		int sum = 0;

		for (int value : intArray) {

			sum += value;

		}
		System.out.println("sum of the numbers:" + sum);

	}

}
