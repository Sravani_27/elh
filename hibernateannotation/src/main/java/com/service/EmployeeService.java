package com.service;

import java.util.List;

import com.bean.Employee;

public interface EmployeeService {
	public abstract Employee createEmployee(Employee employee);

	public abstract List<Employee> readAllEmployees();
	
	public abstract Employee readEmployeeById(int employeeNumber);
	
	public abstract Employee readEmployeeByName(int employeeName);
	
	public abstract Employee updateEmployee(Employee employee);
	
	public abstract int deleteEmployeeById(int employeeNumber);
	
	
}
