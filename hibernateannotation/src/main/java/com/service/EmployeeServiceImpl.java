package com.service;

import java.util.List;

import com.bean.Employee;
import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;

public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee createEmployee(Employee employee) {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		
		return employeeDao.createEmployee(employee);
	}

	@Override
	public List<Employee> readAllEmployees() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeById(int employeeNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee readEmployeeByName(int employeeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteEmployeeById(int employeeNumber) {
		// TODO Auto-generated method stub
		return 0;
	}

}
