package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bean.Employee;
import com.util.HibernateUtil;

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee createEmployee(Employee employee) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(employee);
		transaction.commit();
		return employee;
	}

	@Override
	public List<Employee> readAllEmployees() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		String readAllEmployee = "from Employee";
		Transaction transaction = session.beginTransaction();
		List<Employee> employees = session.createQuery(readAllEmployee).list();
		transaction.commit();
		return employees;
	}

	@Override
	public Employee readEmployeeById(int employeeNumber) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Employee employee = session.load(Employee.class, employeeNumber);

		return employee;
	}

	@Override
	public Employee readEmployeeByName(int employeeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.update(employee);
		transaction.commit();
		return employee;
	}

	@Override
	public int deleteEmployeeById(int employeeNumber) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.delete(session.get(Employee.class, employeeNumber));
		transaction.commit();
		return 1;
	}

}
