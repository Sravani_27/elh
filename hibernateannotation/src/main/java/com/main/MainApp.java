package com.main;

import com.bean.Employee;
import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;

public class MainApp {

	public static void main(String[] args) {

		EmployeeDao employeeDao = new EmployeeDaoImpl();
		Employee employee = new Employee("Jong Seok", 50000f);

		employeeDao.createEmployee(employee);
	}

}
