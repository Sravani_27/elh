package com.springbootvalidation.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

//import javax.validation.Constraint;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootvalidation.exception.CustomError;
import com.springbootvalidation.model.Employee;

@RestController
public class EmployeeController {

	@PostMapping("/add")
	public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee employee) {
		// service or repository
		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		System.out.println(employee.getEmail());
		System.out.println(employee.getDateOfBirth());
		return new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
	}

	@GetMapping("/get")
	public Employee getEmployee() {
		return new Employee(111, "kirby", 22233f, LocalDate.now(), "landon@gmail.com");
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<CustomError> handleException() {

		CustomError customError = new CustomError(LocalDateTime.now(), "Exception Occurred", "Please check");
		return new ResponseEntity<CustomError>(customError, HttpStatus.BAD_REQUEST);

	}
}
