package com.springbootvalidation.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/addnumber/{alias}")
	public int addNumbers(@PathVariable("alias") int num1) {
		int output = 0;
		if (num1 > 0) {
			output = num1 + 100;

		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
		return output;
	}
}
