package com.springbootvalidation.model;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Employee {

	private int empNo;
	@NotEmpty(message = "Please enter employee name..")
	@Size(min = 5,max =15,message= "minimum characters should be three characters..")
	private String empName;
	private double salary;
	@JsonFormat(pattern ="yyyy-MM-dd")
	@Past
	public LocalDate dateOfBirth;
	@Email
	private String email;

	public Employee() {
		super();
	}

	public Employee(int empNo, String empName, double salary, LocalDate dateOfBirth, String email) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.dateOfBirth = dateOfBirth;
		this.email = email;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
