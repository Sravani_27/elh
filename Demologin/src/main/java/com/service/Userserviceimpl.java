package com.service;

import com.dao.Userdao;
import com.dao.Userdaoimpl;
import com.model.User;

public class Userserviceimpl implements Userservice {

	@Override
	public User createUser(User user) {

		return null;
	}

	@Override
	public User readUserById(int userId) {

		return null;
	}

	@Override
	public User updateUser(User user) {

		return null;
	}

	@Override
	public int deleteUserByUserId(int userId) {

		return 0;
	}

	@Override
	public User validateUserAndPassword(int userId, String password) {

		User user = null;
		//parent type reference 
		Userdao userdao = new Userdaoimpl();
		// converting integer to string and returning length of the integer
		int length = String.valueOf(userId).length();

		if (length > 5 && password.length() > 5) {
			user = userdao.validateUserAndPassword(userId, password);
		}
		else {
		     user = null;
		     
		}

		return user;
	}

}
