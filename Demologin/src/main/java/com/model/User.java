package com.model;

public class User {

	private int userId;
	private String UserName;
	private String password;

	public User() {
		super();

	}

	public User(int userId, String userName, String password) {
		super();
		this.userId = userId;
		UserName = userName;
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
