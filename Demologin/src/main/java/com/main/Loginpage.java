package com.main;

import java.util.Scanner;

import com.model.User;
import com.service.Userservice;
import com.service.Userserviceimpl;

public class Loginpage {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter userId: ");
		int userId = scanner.nextInt();
		System.out.println("Enter your password: ");
		String password = scanner.next();

		Userservice userservice = new Userserviceimpl();
		User user = userservice.validateUserAndPassword(userId, password);

		if (user != null) {
			System.out.println("user name: " + user.getUserName());
			System.out.println("user id: " + user.getUserId());
			// System.out.println("user password: " + user.getPassword());
		} 
		else 
		{
			System.err.println("Invalid Credentials");
		}

		scanner.close();
		userservice = null;

	}

}
