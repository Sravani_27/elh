package com.main;

import com.model.ThreadDemo;
import com.model.ThreadDemo1;

public class MainThread {

	public static void main(String[] args) {

		ThreadDemo threadDemo = new ThreadDemo();
		threadDemo.start();
		System.out.println(threadDemo.getPriority());// Default thread priority is 5
		threadDemo.setPriority(4);
		System.out.println(threadDemo.getPriority());
		try {
			threadDemo.sleep(30000);
			System.out.println("Abc");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		threadDemo = null;

		// Runnable interface
		ThreadDemo1 threadDemo1 = new ThreadDemo1();
		new Thread(threadDemo1).start(); // without Thread class we cannot call Runnable interface

		threadDemo1 = null;
	}

}
