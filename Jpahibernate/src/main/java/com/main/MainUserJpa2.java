package com.main;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.dao.UserDao;
import com.model.User;

public class MainUserJpa2 {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		// invoke the Dao class
		UserDao userDao = new UserDao(entityManager);// constructor injection
		/*
		 * User user = new User(); //user.setId(new Integer(12)); // wrapper classes
		 * convert primitive data type to object user.setName("Cat");
		 * 
		 * Optional<User> optional = userDao.save(user);
		 * 
		 * if (optional.isPresent()) { System.out.println(optional.get().getId());
		 * System.out.println(optional.get().getName()); } else {
		 * System.out.println("OOps Error...!"); }
		 */

		Optional<User> optional = userDao.findById(2);
		User user = optional.get();
		System.out.println(user.getId());
		System.out.println(user.getName());
	}

}
