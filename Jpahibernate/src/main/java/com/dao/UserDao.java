package com.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.model.User;

public class UserDao {

	private EntityManager entityManager;// has a relation

	public UserDao(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	public Optional<User> save(User user) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(user); // session.save(object)
			entityManager.getTransaction().commit();
			return Optional.of(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	public Optional<User> findById(Integer id) {
		User user = entityManager.find(User.class, id);
		return user != null ? Optional.of(user) : Optional.empty();
	}

	public List<User> findAll() {
		return entityManager.createQuery("from User").getResultList();
	}

	// Read back the object
	// Note: you should execute find() and remove() in the same transaction

	public Optional<User> update(User user) {
		try {
			User loadUser = entityManager.find(User.class, user.getId());
			entityManager.getTransaction().begin();
			entityManager.merge(user);
			entityManager.getTransaction().commit();
			return Optional.of(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	public Integer deleteById(Integer id) {
		User user = entityManager.find(User.class, id);
		entityManager.getTransaction().begin();
		entityManager.remove(user);
		entityManager.getTransaction().commit();

		return id;
	}

}
