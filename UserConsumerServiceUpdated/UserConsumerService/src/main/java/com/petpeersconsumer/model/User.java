package com.petpeersconsumer.model;

/**
 * @author yerramreddy.sravani
 */
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pet_user")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1418608533596716706L;

	@Id
	@Column(nullable = false, length = 2)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "user_name", nullable = false, unique = true, length = 55)
	private String username;

	@Column(name = "user_passwd", nullable = false, length = 55)
	private String userPassword;

	@Transient
	private String confirmPassword;

	@JsonIgnoreProperties
	@Transient
	private Set<PetDTO> pets;

	@Column(name = "Pet_Id")
	private int petId;
}
