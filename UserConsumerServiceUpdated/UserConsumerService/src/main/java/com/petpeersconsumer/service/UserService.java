package com.petpeersconsumer.service;

import java.util.List;
import java.util.Set;

import com.petpeersconsumer.model.PetDTO;
import com.petpeersconsumer.model.User;

/**
 * 
 * @author yerramreddy.sravani
 *
 */
public interface UserService {

	public abstract User addUser(User user);

	public abstract User updateUser(User use);

	public abstract List<User> listUsers();

	public abstract User getUserById(int id);

	public abstract String removeUser(int id);

	public abstract User findByUserName(String name);

	public abstract User getMyPets(int userid);

	public abstract User aunthenticateUser(String name, String password, String confimPassword);

	public abstract int saveUser(User user);

	public abstract PetDTO buyPet(int petId, int userId);
}
