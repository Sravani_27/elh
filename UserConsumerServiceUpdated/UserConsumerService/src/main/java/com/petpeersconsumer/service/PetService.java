package com.petpeersconsumer.service;

import java.util.List;
import java.util.Set;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.petpeersconsumer.model.PetDTO;

/**
 * 
 * @author yerramreddy.sravani
 *
 */

@FeignClient(name = "PET-SERVICE", url = "http://localhost:8081/pets")
//@FeignClient(name = "http://PET-SERVICE/pets")
public interface PetService {

	@RequestMapping("/hello")
	public abstract String sayHello();

	@GetMapping("/")
	public abstract List<PetDTO> petHome();

	@GetMapping("/myPets")
	public abstract ResponseEntity<List<PetDTO>> myPets();

	@GetMapping("/petDetail")
	public abstract ResponseEntity<List<PetDTO>> petDetails();

	@PostMapping("/addPet")
	public abstract PetDTO addPet(@RequestBody PetDTO pet);

	@PutMapping("/buyPet/{petId}")
	public abstract PetDTO buyPet(@PathVariable int petId);

	@GetMapping("/id/{id}")
	public abstract Set<PetDTO> getByOwnerId(@PathVariable("id") int id);
}
