package com.petpeersconsumer.validator;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.petpeersconsumer.model.PetDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author yerramreddy.sravani
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class UserValidator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2917812425236080768L;

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters long")
	private String userName;

	@NotEmpty(message = "Password may not be empty")
	@Size(min = 2, max = 55, message = "password must be between 2 and 55 characters long")
	private String userPassword;

	private transient String confirmPassword;

	private Set<PetDTO> pets;
}
