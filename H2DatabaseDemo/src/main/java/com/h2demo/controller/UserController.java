package com.h2demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.h2demo.model.User;
import com.h2demo.service.UserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Validated
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/create")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		log.info("in the controoler layer");
		User user1 = userService.createUser(user);

		return new ResponseEntity<User>(user1, HttpStatus.CREATED);
	}

	@GetMapping("/read/{userId}")
	public ResponseEntity<User> getUserById(@PathVariable int userId) {
		log.info(" get user by id menthod in the controoler layer");
		User user2 = userService.readUserById(userId);
		return new ResponseEntity<User>(user2, HttpStatus.FOUND);
	}

	@PostMapping("/update")
	public ResponseEntity<User> updateUser(@RequestBody User user) {
		log.info("in the controoler layer");
		User user1 = userService.updateUser(user);

		return new ResponseEntity<User>(user1, HttpStatus.CREATED);
	}

	@GetMapping("/readall")
	public ResponseEntity<List<User>> readAll() {
		log.info("in the controoler layer");
		return new ResponseEntity<List<User>>(userService.readAllUsers(), HttpStatus.FOUND);
	}

	@GetMapping("/hi/{name}")
	public ResponseEntity<String> sayHi(
			@PathVariable @Size(min = 3, message = "name should have atleast 3 characters") String name) {
		log.info("inside the hi method in the controller layer" + name);
		return new ResponseEntity<String>(name + "welcome!", HttpStatus.CHECKPOINT);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleConstraintVoilation(ConstraintViolationException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(cv -> {
			errors.put("message", cv.getMessage());
			errors.put("path", (cv.getPropertyPath()).toString());
		});
		return errors;
	}
}
