package com.h2demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.h2demo.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
