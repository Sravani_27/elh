package com.h2demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.h2demo.model.User;
import com.h2demo.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public User createUser(User user) {

		log.info("adduser in userservice layer");
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public User readUserById(int userId) {
		log.info("finduser by id in userservice layer");
		User user1 = null;
		Optional<User> optional = userRepository.findById(userId);
		if (optional.isPresent()) {
			user1 = optional.get();
		}
		return user1;
	}

	@Override
	@Transactional
	public List<User> readAllUsers() {
		log.info("read user in userservice layer");
		return userRepository.findAll();
	}

}
