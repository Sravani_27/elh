package com.main;

import com.exception.UserException;
import com.model.User;

import com.service.UserService;
import com.service.UserServiceImpl;

public class LoginMainApp {

	public static void main(String[] args) throws UserException {

		UserService userService = new UserServiceImpl();
		User data;

		data = userService.validateUserAndPassword(11223344, "asdfghrt");
		if (data != null) {
			System.out.println("User number: " + data.getUserId());
			System.out.println("User name: " + data.getUserName());
			System.out.println("User password: " + data.getUserPassword());

		}
		else {
			System.err.println("Data is not available");
		}

	}

}
