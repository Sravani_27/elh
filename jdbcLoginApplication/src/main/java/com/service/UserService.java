package com.service;

import com.exception.UserException;
import com.model.User;

public interface UserService {
	
	public abstract User createUser(User user);

	public abstract User readUserById(int userId);

	public abstract User updateUser(User user);

	public abstract int deleteUserByUserId(int userId);

	public abstract User validateUserAndPassword(int userId, String password)throws UserException;

}
