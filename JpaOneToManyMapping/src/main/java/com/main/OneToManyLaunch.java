package com.main;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.model.Department;
import com.model.Employee;

public class OneToManyLaunch {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("employees_list");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		try {
			entityManager.getTransaction().begin();

			Employee employee1 = new Employee("Crow");
			Employee employee2 = new Employee("Chick");
			Employee employee3 = new Employee("Camel");

			entityManager.persist(employee1);
			entityManager.persist(employee2);
			entityManager.persist(employee3);

			List<Employee> employeesList = new ArrayList<>();
			employeesList.add(employee1);
			employeesList.add(employee2);
			employeesList.add(employee3);

			Employee employee11 = new Employee("Cucumber");
			Employee employee22 = new Employee("Carrot");
			Employee employee33 = new Employee("Caramel");

			entityManager.persist(employee11);
			entityManager.persist(employee22);
			entityManager.persist(employee33);

			List<Employee> employeesList1 = new ArrayList<>();
			employeesList1.add(employee11);
			employeesList1.add(employee22);
			employeesList1.add(employee33);

			Department department = new Department("Developer", employeesList);

			entityManager.persist(department);

			Department department1 = new Department("Tester", employeesList1);

			entityManager.persist(department1);

		} catch (Exception e) {
			System.out.println("An exception occured: " + e);
			e.printStackTrace();
		} finally {

			entityManager.getTransaction().commit();
			try {
				entityManager.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				entityManagerFactory.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
