package com.model;

//import java.util.Comparator;

public class Employee implements Comparable<Employee> {

	private int employeeId;
	private String employeeName;
	private float salary;

	public Employee() {
		super();

	}

	public Employee(int employeeId, String employeeName, float salary) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.salary = salary;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	/*
	 * @Override public int compare(Employee o1, Employee o2) {
	 * 
	 * if (o1.getEmployeeId() < o2.getEmployeeId()) { return -1; } else if
	 * (o1.getEmployeeId() > o2.getEmployeeId()) { return 1; } else { return 0; }
	 * 
	 * }
	 */

	//sorting by employeenumber
/*	@Override
	public int compareTo(Employee o) {
 
		return this.employeeId - o.getEmployeeId();
	}*/
	
	//sorting by employee salary
	@Override
	public int compareTo(Employee o) {
 
		return (int) (this.salary - o.getSalary());
	}

}
