package com.model;

import java.util.Comparator;

public class CompareById implements Comparator<EmployeePojo> {

	@Override
	public int compare(EmployeePojo o1, EmployeePojo o2) {

		if (o1.getEmployeeId() < o2.getEmployeeId()) {
			return -1;
		} else if (o1.getEmployeeId() > o2.getEmployeeId()) {
			return 1;
		} else {
			return 0;
		}
	}

}
