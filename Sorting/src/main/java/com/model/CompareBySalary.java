package com.model;

import java.util.Comparator;

public class CompareBySalary implements Comparator<EmployeePojo> {

	@Override
	public int compare(EmployeePojo o1, EmployeePojo o2) {

		if (o1.getSalary() < o2.getSalary()) {
			return -1;
		} else if (o1.getSalary() > o2.getSalary()) {
			return 1;
		} else {
			return 0;
		}
	}

}
