package com.model;

import java.util.Comparator;
//import java.lang.Comparable;

public class Compare implements Comparator<EmployeePojo> {

	@Override
	public int compare(EmployeePojo o1, EmployeePojo o2) {
		
		return o1.getEmployeeName().compareTo(o2.getEmployeeName());
	}

}
