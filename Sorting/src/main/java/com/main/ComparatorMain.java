package com.main;

import java.util.Set;
import java.util.TreeSet;

import com.model.Employee;

public class ComparatorMain {

	public static void main(String[] args) {
		Employee employee1 = new Employee(1234567, "Parrot", 34211f);
		Employee employee2 = new Employee(7654321, "Pigeon", 22311f);
		Employee employee3 = new Employee(1980967, "Shin Chan", 25726f);
		Employee employee4 = new Employee(3326217, "Oggy", 44313f);

		Set<Employee> employees = new TreeSet<>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);

		for (Employee employee : employees) {
			System.out.println(employee.getEmployeeId());
			System.out.println(employee.getEmployeeName());
			System.out.println(employee.getSalary());
		}

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employee4 = null;
		employees = null;
	}

}
