package com.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.model.Compare;
import com.model.CompareById;
import com.model.CompareBySalary;
import com.model.EmployeePojo;

public class Comparing {

	public static void main(String[] args) {
		EmployeePojo employee1 = new EmployeePojo(1234567, "Parrot", 34211f);
		EmployeePojo employee2 = new EmployeePojo(7654321, "Pigeon", 22311f);
		EmployeePojo employee3 = new EmployeePojo(1980967, "Shin Chan", 25726f);
		EmployeePojo employee4 = new EmployeePojo(3326217, "Oggy", 44313f);

		List<EmployeePojo> employees = new ArrayList<>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
		
		//sorting by name
		Collections.sort(employees, new Compare());
		for (EmployeePojo employee : employees) {
			System.out.println("Sorting by employee name:  ");
			System.out.println(employee.getEmployeeId());
			System.out.println(employee.getEmployeeName());
			System.out.println(employee.getSalary());
		}
		
		//sorting by employee number
		Collections.sort(employees, new CompareById());
		for (EmployeePojo employee : employees) {
			System.out.println("Sorting by employee number:  ");
			System.out.println(employee.getEmployeeId());
			System.out.println(employee.getEmployeeName());
			System.out.println(employee.getSalary());
		}
		
		//sorting by salary
		Collections.sort(employees, new CompareBySalary());
		
		for (EmployeePojo employee : employees) {
			System.out.println("Sorting by employee salary:  ");
			System.out.println(employee.getEmployeeId());
			System.out.println(employee.getEmployeeName());
			System.out.println(employee.getSalary());
		}

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employee4 = null;
		employees = null; 
		

	}

}
