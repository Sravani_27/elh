package com.springbootdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Address_Table")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Door_Number")
	private int doorNumber;
	@Column(name = "City", length = 25)
	private String city;
	@Column(name = "State", length = 25)
	private String state;

	public Address() {
		super();
	}

	public Address(int doorNumber, String city, String state) {
		super();
		this.doorNumber = doorNumber;
		this.city = city;
		this.state = state;
	}

	public int getDoorNumber() {
		return doorNumber;
	}

	public void setDoorNumber(int doorNumber) {
		this.doorNumber = doorNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
