package com.springbootdemo.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.model.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {

	public Optional<Address> findAddressByCity(String city);

	public Optional<Address> findAddressByState(String state);
}
