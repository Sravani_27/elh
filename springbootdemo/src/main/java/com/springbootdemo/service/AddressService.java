package com.springbootdemo.service;

import com.springbootdemo.model.Address;

public interface AddressService {
	
	public abstract Address addAddress(Address address);
	
	public abstract Address readAddressByCity(String city);
	
	public abstract Address readAddressByState(String state);
	
	public abstract Address readAddressByDoorNumber(int doorNumber);
	
	public abstract Address alterAddress(Address address);
	
	
	public abstract int deleteAddress(int doorNumber);
	
	//public abstract Address addAddress(Address address);
}
