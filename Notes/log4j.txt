log4j:

in any web application/standalone please avoid using s.o.p's

instead use log4j

step: create a project

step2:
set application.properties

logging.file.name=<filename>.log
logging.pattern.rolling-file-name=MyApp-%d{yyyy-MM-dd}.%i.log
logging.file.max-size=1MB
logging.file.total-size-cap=10MB
logging.file.max-history=30
logging.file.clean-history-on-start=true