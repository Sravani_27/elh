package com.hcl.pp.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.InvalidLoginException;
import com.hcl.pp.exception.PetSoledOutFoundException;
import com.hcl.pp.exception.UserNameAlreadyExist;
import com.hcl.pp.exception.UserNameAndPasswordSame;
import com.hcl.pp.exception.UserNotFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;

@RestController
@Validated
public class PetsAppController {

	@Autowired
	private UserService userService;

	@Autowired
	private PetService petService;

	@PostMapping("/user/add")
	public ResponseEntity<User> addUser(@RequestBody @Valid User user) throws UserNameAndPasswordSame {
		User user1 = null;
		try {
			if (user != null) {

				if (user.getUsername().equalsIgnoreCase(user.getUserPassword())) {

					throw new UserNameAndPasswordSame("username and password should not be same");
				} else {
					if (user.getUserPassword().equals(user.getConfirmPassword())) {
						user1 = userService.addUser(user);
						return new ResponseEntity<User>(user1, HttpStatus.CREATED);
					} else {

						throw new UserNotFoundException("Password do not match");
					}
				}
			} else {
				throw new UserNotFoundException("please enter");
			}
		} catch (UserNameAndPasswordSame e) {
			System.err.println(e);
		}
		return new ResponseEntity<User>(user1, HttpStatus.CREATED);
	}

	@GetMapping("/user/login")
	public ResponseEntity<User> loginUser(@Valid @RequestParam("name") String name,
			@RequestParam("password") String password, @RequestParam("confirmPassword") String confirmPassword) {
		User user = userService.authenticateUser(name, password, confirmPassword);

		if (name.equals(password)) {

			throw new InvalidLoginException("username and password must not be same");

		} else {
			if (password.equals(confirmPassword)) {
				user = userService.authenticateUser(name, password, confirmPassword);
			} else {

				throw new InvalidLoginException("password and confirm password should be same");
			}
		}
		return new ResponseEntity<User>(user, HttpStatus.FOUND);

	}

	@GetMapping("/user/logout")
	public ResponseEntity<String> logout() {
		return new ResponseEntity<String>("Please login!!", HttpStatus.OK);
	}

	@GetMapping("/pets/myPets")
	public ResponseEntity<List<Pet>> myPets() {
		List<Pet> pet = petService.fetchAll();
		return new ResponseEntity<List<Pet>>(pet, HttpStatus.FOUND);
	}

	@GetMapping("/pets/petDetail")
	public ResponseEntity<List<Pet>> petDetails() {
		List<Pet> pet = petService.getAllPets();
		return new ResponseEntity<List<Pet>>(pet, HttpStatus.FOUND);
	}

	@PutMapping("/pets/buyPet/{petId}")
	public Pet buyPet(@PathVariable int petId) {
		Pet pet = null;
		try {
			pet = userService.buyPet(petId);
		} catch (PetSoledOutFoundException e) {
			e.printStackTrace();
		}

		return pet;

	}

	@PostMapping("/pets/addPet")
	public ResponseEntity<Pet> addPet(@RequestBody Pet pet) {
		return new ResponseEntity<Pet>(petService.savePet(pet), HttpStatus.CREATED);
	}

	@GetMapping("/pets")
	public List<Pet> petHome() {

		return petService.getAllPets();
	}

	
}
