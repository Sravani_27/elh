package com.petpeersproducer.controller;

/**
 * @author yerramreddy.sravani
 */
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.petpeersproducer.model.Pet;
import com.petpeersproducer.service.PetService;
import com.petpeersproducer.service.UserService;

@RestController
@RequestMapping("/pets")
public class PetController {

	@Autowired
	private PetService petService;

	@Autowired
	private UserService userService;

	@RequestMapping("/hello")
	public ResponseEntity<String> sayHello() {
		return new ResponseEntity<String>("Hello There!", HttpStatus.OK);
	}

	// ----------------------------------------------------------//

	@PostMapping("/addPet")
	public ResponseEntity<Pet> addPet(@Valid @RequestBody Pet pet) {
		ResponseEntity<Pet> responseEntity = null;
		if (pet != null) {
			responseEntity = new ResponseEntity<Pet>(petService.savePet(pet), HttpStatus.CREATED);
		} else {
			responseEntity = new ResponseEntity<Pet>(petService.savePet(pet), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@GetMapping("/getPets")
	public ResponseEntity<List<Pet>> findAllPets() {
		return new ResponseEntity<List<Pet>>(petService.getAllPets(), HttpStatus.ACCEPTED);

	}

	@GetMapping("/")
	public List<Pet> petHome() {
		return petService.getAllPets();
	}

	@GetMapping("/myPets")
	public ResponseEntity<List<Pet>> myPets() {
		return new ResponseEntity<List<Pet>>(petService.fetchAll(), HttpStatus.FOUND);
	}

	@GetMapping("/petDetail")
	public ResponseEntity<List<Pet>> petDetails() {
		return new ResponseEntity<List<Pet>>(petService.fetchAll(), HttpStatus.FOUND);
	}

	@PutMapping("/buyPet/{petId}")
	public Pet buyPet(@PathVariable int petId) {
		Pet pet = petService.getId(petId);
		return pet;
	}

	@GetMapping("/id/{ownerId}")
	public Set<Pet> getByOwnerId(@PathVariable("ownerId") int ownerId) {

		Set<Pet> listPet = petService.getByOwnerId(ownerId);
		return listPet;
		// return new ResponseEntity<>(listPet, HttpStatus.OK);
	}
}
