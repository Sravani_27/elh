package com.petpeersproducer.validator;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class PetValidator implements Serializable {

	/**
	 * @author yerramreddy.sravani
	 */
	private static final long serialVersionUID = -1224360391359465962L;

	@NotNull(message = "Pet age can't be null")
	@Digits(integer = 2, fraction = 0, message = "Age must be in number")
	@Min(value = 0, message = "Age must be minimum 0years")
	@Max(value = 99, message = "Age must be maximum upto 99 years")
	private int age;

	@NotNull(message = "Pet Name can't be null")
	@NotEmpty(message = "Pet Name can't be empty")
	private String name;

	@NotNull(message = "Place can't be null")
	@NotEmpty(message = "Place can't be empty")
	private String place;

}
