package com.petpeersproducer.service;

/**
 * @author yerramreddy.sravani
 */
import java.util.List;
import java.util.Set;

import com.petpeersproducer.model.Pet;

public interface PetService {

	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getAllPets();

	public abstract List<Pet> fetchAll();

	public abstract Pet buyPet(Pet pet);

	public abstract Pet getId(int id);
	
	public abstract Set<Pet> getByOwnerId(int ownerId);
}
