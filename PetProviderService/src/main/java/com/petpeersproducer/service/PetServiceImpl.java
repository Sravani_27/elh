package com.petpeersproducer.service;

/**
 * @author yerramreddy.sravani
 */
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petpeersproducer.model.Pet;
import com.petpeersproducer.repository.PetRepository;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Override
	@Transactional
	public Pet savePet(Pet pet) {
		Pet newPet = null;

		Pet pet1 = new Pet();
		pet1.setName(pet.getName());
		pet1.setAge(pet.getAge());
		pet1.setPlace(pet.getPlace());
		pet1.setOwner(pet.getOwner());

		if (pet1 != null) {
			newPet = petRepository.save(pet1);
		}

		return newPet;
	}

	@Override
	@Transactional
	public List<Pet> getAllPets() {

		return petRepository.findAll();
	}

	@Override
	@Transactional
	public List<Pet> fetchAll() {

		return petRepository.findAll();
	}

	@Override
	@Transactional
	public Pet buyPet(Pet pet) {
		Optional<Pet> pet1 = petRepository.findById(pet.getId());
		Pet pet2 = pet1.get();
		Pet pet3 = petRepository.save(pet2);
		return pet3;
	}

	@Override
	@Transactional
	public Pet getId(int id) {
		Optional<Pet> pet1 = petRepository.findById(id);
		Pet pet = pet1.get();
		return pet;
	}

	@Override
	@Transactional
	public Set<Pet> getByOwnerId(int ownerId) {

		return petRepository.findByowner(ownerId);
	}

}
