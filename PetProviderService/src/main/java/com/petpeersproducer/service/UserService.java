package com.petpeersproducer.service;

/**
 * @author yerramreddy.sravani
 */
import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.petpeersproducer.model.UserDTO;

@FeignClient(name = "USER-SERVICE", url = "http://localhost:8082/user")
//@FeignClient(name = "http://USER-SERVICE/user")
public interface UserService {

	@PostMapping("/add")
	public abstract ResponseEntity<UserDTO> addUser(@RequestBody @Valid UserDTO user);

	@GetMapping("/login")
	public abstract ResponseEntity<Object> loginUser(@RequestParam("name") String name,
			@RequestParam("password") String password, @RequestParam("confimPassword") String confimPassword);

	@GetMapping("/logout")
	public abstract ResponseEntity<String> logout();
	
	
}
