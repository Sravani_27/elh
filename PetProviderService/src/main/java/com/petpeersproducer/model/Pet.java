package com.petpeersproducer.model;

/**
 * @author yerramreddy.sravani
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pets")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Pet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7056953971932729677L;

	@Id
	@Column(nullable = false, length = 2)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "pet_name", nullable = false, length = 55)
	private String name;

	@Column(name = "pet_age", length = 2)
	private int age;

	@Column(name = "pet_place", length = 55)
	private String place;

	// @JoinColumn(name = "pet_ownerid")
	// @ManyToOne(fetch = FetchType.EAGER)
	// @ManyToOne(cascade = CascadeType.ALL)
	@Column(name = "pet_ownerid")
	private int owner;

}
