package com.petpeersproducer.model;

/**
 * @author yerramreddy.sravani
 */
import java.util.Set;

import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

	private int id;

	private String username;

	private String userPassword;

	@Transient
	private String confirmPassword;

	// @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	// @JsonIgnoreProperties({ "owner" })
	@Transient
	private Set<Pet> pets;
}
