package com.consumerfeign.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.consumerfeign.model.Department;
import com.consumerfeign.model.EmployeeDto;
import com.consumerfeign.service.ProducerClient;

@RestController
@RequestMapping("/deparment")
public class ConsumerController {

	@Autowired
	private ProducerClient producerClient;

	@GetMapping("/consumer")
	@HystrixCommand(fallbackMethod = "sorry")
	public ResponseEntity<String> getForm() {
		return producerClient.check();
	}

	@PostMapping("/createDepartment")
	public ResponseEntity<Department> createEmployee(@RequestBody Department department) {
		System.out.println(department);
		return new ResponseEntity<Department>(department, HttpStatus.OK);
	}

	// not work
	@GetMapping("/readEmployee")
	@HystrixCommand(fallbackMethod = "welcome")
	public ResponseEntity<List<Department>> readEmployee() {
		List<EmployeeDto> employees1 = producerClient.anyFunctionName(1);
		Department department1 = new Department(1, "Developer", employees1);

		List<EmployeeDto> employees2 = producerClient.anyFunctionName(1);
		Department department2 = new Department(2, "Developer", employees2);

		List<Department> department = new ArrayList<>();
		department.add(department1);
		department.add(department2);

		return new ResponseEntity<List<Department>>(department, HttpStatus.OK);
	}

	@GetMapping("/readEmployee/{deptID}")
	@HystrixCommand(fallbackMethod = "welcome")
	public ResponseEntity<List<EmployeeDto>> readEmployeeByID(@PathVariable int deptID) {
		List<EmployeeDto> department = (List<EmployeeDto>) producerClient.anyFunctionName(deptID);
		return new ResponseEntity<List<EmployeeDto>>(department, HttpStatus.OK);
	}

	public ResponseEntity<String> sorry() {
		return new ResponseEntity<String>("please contact admin", HttpStatus.EXPECTATION_FAILED);
	}

	public ResponseEntity<String> welcome() {
		return new ResponseEntity<String>("please contact me", HttpStatus.EXPECTATION_FAILED);
	}
}
