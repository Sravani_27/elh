package com.consumerfeign.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto implements Serializable {
	private static final long serialVersionUID = 2616333472600854894L;

	private int empNo;
	private String name;
	private float salary;
	private int deptId;
}
