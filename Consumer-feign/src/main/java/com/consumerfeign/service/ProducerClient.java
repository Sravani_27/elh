package com.consumerfeign.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.consumerfeign.model.EmployeeDto;

//@FeignClient(name = "simple-Ref", url = "http://localhost:9090/employee")
@FeignClient(name ="PRODUCER/employee")
public interface ProducerClient {

	@GetMapping("/hai")
	public abstract ResponseEntity<String> check();

	@GetMapping("/readEmployee")
	public abstract List<EmployeeDto> getAllEmployee();

	@PostMapping("/createEmployee")
	public abstract ResponseEntity<EmployeeDto> addEmployee(@RequestBody EmployeeDto employeeDto);

	@GetMapping("/readEmployee/{deptID}")
	public abstract List<EmployeeDto> anyFunctionName(@PathVariable int deptID);

}