package com.hystrix.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/main")
public class MyController {

	@GetMapping("/hi")
	public String sayHello() {
		return "Welcome to Hystix Rest end point";
	}
}
