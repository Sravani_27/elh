<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>spring boot form submit example</title>
</head>
<body>
<h1>spring boot form submit example</h1>
<h2> Details as submitted successfully </h2>
<h4> Employee Name : ${employeeName} </h4>
<h4> Employee Email : ${employeeEmail} </h4>
</body>
</html>