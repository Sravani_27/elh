package com.petpeersconsumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.petpeersconsumer.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public User findByUsernameAndUserPassword(String name, String password);

	public User findByUsername(String name);

	@Query(value = "UPDATE PET_USER1 as u SET  u.id= :userId, u.user_passwd= :userPassword, u.petid=:petid  WHERE u.user_name = :userName", nativeQuery = true)
	public void update(@Param("userId") int userId, @Param("userPassword") String userPassword,
			@Param("petid") int petid, @Param("userName") String userName);

}
