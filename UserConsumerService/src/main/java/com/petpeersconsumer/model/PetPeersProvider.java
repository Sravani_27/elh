package com.petpeersconsumer.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author yerramreddy.sravani
 *
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PetPeersProvider {

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private User user;

	private Set<PetDTO> pets;
}
