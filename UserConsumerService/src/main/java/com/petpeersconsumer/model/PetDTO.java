package com.petpeersconsumer.model;

/**
 * @author yerramreddy.sravani
 */
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PetDTO {

	private int id;

	private String name;

	private int age;

	private String place;

	private int petOwnerId;
}
