package com.petpeersconsumer.validator;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author yerramreddy.sravani
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class LoginValidator implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1781535947542734821L;

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters long")
	private String userName;

	@NotEmpty(message = "Password may not be empty")
	@Size(min = 2, max = 55, message = "password must be between 2 and 55 characters long")
	private String userPassword;

	@NotEmpty(message = "confirmPassword may not be empty")
	@Size(min = 2, max = 55, message = "password and confirmPassword Not Matched")
	private String confirmPassword;

}
