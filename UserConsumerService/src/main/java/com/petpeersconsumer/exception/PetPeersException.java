package com.petpeersconsumer.exception;

/**
 * 
 * @author yerramreddy.sravani
 *
 */

public class PetPeersException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4520031582441231144L;
	private String massage;

	public PetPeersException() {
		super();
	}

	public PetPeersException(String massage) {
		// super(String.format(" '%s'", massage));
		super();
		this.massage = massage;
	}

	public String getMassage() {
		return massage;
	}

}
