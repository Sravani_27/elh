package com.petpeersconsumer.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.petpeersconsumer.exception.PetPeersException;
import com.petpeersconsumer.model.PetDTO;
import com.petpeersconsumer.model.PetPeersProvider;
import com.petpeersconsumer.model.User;
import com.petpeersconsumer.service.PetService;
import com.petpeersconsumer.service.UserService;
import com.petpeersconsumer.validator.LoginValidator;
import com.petpeersconsumer.validator.PetValidator;
import com.petpeersconsumer.validator.UserValidator;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
@Validated
public class UserController {

	@Autowired
	private PetService petService;

	@Autowired
	private UserService userService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	UserValidator userValidator;

	@Autowired
	LoginValidator loginValidator;

	@Autowired
	PetValidator petValidator;

	@RequestMapping("/hi")
	public ResponseEntity<String> helloMethod() {
		return new ResponseEntity<>(petService.sayHello(), HttpStatus.OK);
	}

	@GetMapping("/myPets/{id}")
	public ResponseEntity<PetPeersProvider> myPets(@PathVariable("id") int id) {
		PetPeersProvider petPeers = new PetPeersProvider();
		// first get user details
		User user = userService.getMyPets(id);
		petPeers.setUser(user);
		// get all pets user having
		// Set<PetDTO> listOfPets =
		// restTemplate.getForObject("http://PET-SERVICE/pets/id/" + id, Set.class);
		Set<PetDTO> listOfPets = petService.getByOwnerId(id);
		petPeers.setPets(listOfPets);
		return new ResponseEntity<PetPeersProvider>(petPeers, HttpStatus.FOUND);
	}

	@PutMapping("/pets/buyPet/{petId}")
	public PetDTO buyPet(@PathVariable int petId, @RequestParam int userId) {
		PetDTO pet = null;

		pet = userService.buyPet(petId, userId);

		return pet;

	}

	@PostMapping("/add")
	public ResponseEntity<User> addUser(@RequestBody @Valid User user) {
		User user1 = null;
		if (user != null) {
			userValidator.setUserName(user.getUsername());
			userValidator.setUserPassword(user.getUserPassword());
			userValidator.setConfirmPassword(user.getConfirmPassword());

			String name = userValidator.getUserName();
			String password = userValidator.getUserPassword();
			String confimPassword = userValidator.getConfirmPassword();

			if (name.equalsIgnoreCase(password)) {
				log.info("username and password must not be same");
				throw new PetPeersException("username and password must not be same");
			} else {
				if (password.equals(confimPassword)) {
					int id = userService.saveUser(user);
					if (id != 0) {
						int id2 = 0;
						Set<PetDTO> listPet = user.getPets();
						for (PetDTO pet : listPet) {
							PetDTO pp = new PetDTO();
							pp.setName(pet.getName());
							pp.setAge(pet.getAge());
							pp.setPlace(pet.getPlace());
							pp.setPetOwnerId(id);
							id2 = petService.addPet(pp).getId();
						} // won't work for multiple pets per one user

						if (id2 != 0) {
							user.setPetId(id2);
							user.setId(id);
							user1 = userService.addUser(user);
						}
					}
					return new ResponseEntity<User>(user1, HttpStatus.CREATED);
				} else {
					log.info("Password do no match");
					throw new PetPeersException("Password do no match");
				}
			}
		} else {
			throw new PetPeersException("please enter");
		}
	}

	@GetMapping("/login")
	public ResponseEntity<Object> loginUser(@RequestParam("name") String name1,
			@RequestParam("password") String password1, @RequestParam("confimPassword") String confimPassword1) {

		loginValidator = new LoginValidator();
		loginValidator.setUserName(name1);
		loginValidator.setUserPassword(password1);
		loginValidator.setConfirmPassword(confimPassword1);

		String name = loginValidator.getUserName();
		String password = loginValidator.getUserPassword();
		String confimPassword = loginValidator.getConfirmPassword();
		List<PetDTO> pet = null;
		User user = null;
		if (name.equals(password)) {
			log.info("username and password must not be same");
			throw new PetPeersException("username and password must not be same");
		} else {
			if (password.equals(confimPassword)) {
				user = userService.aunthenticateUser(name, password, confimPassword);
				if (user != null) {
					pet = petService.petHome();
					return new ResponseEntity<>(pet, HttpStatus.FOUND);
				}
			} else {
				log.info("password and confimPassword should be same");
				throw new PetPeersException("password and confimpassword should be same");
			}
		}
		return new ResponseEntity<>(user, HttpStatus.FOUND);
	}

	@GetMapping("/logout")
	public ResponseEntity<String> logout() {
		log.info("PersAppController.logout()");
		return new ResponseEntity<String>("please login...", HttpStatus.ACCEPTED);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleConstraintViolation(ConstraintViolationException ex) {
		log.info("UserController.handleConstraintViolation()");
		Map<String, String> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(cv -> {
			errors.put("message", cv.getMessage());
			errors.put("path", (cv.getPropertyPath()).toString());
		});
		return errors;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, String> dataIntegrityViolationException(HttpServletRequest req,
			DataIntegrityViolationException ex) {
		log.info("User Name is already used. please select  a different User Name");
		Map<String, String> errors = new HashMap<>();
		errors.put("message", "User Name is already used. please select  a different User Name");
		errors.put("path", req.getContextPath().toString());
		return errors;
	}

	@ExceptionHandler(value = PetPeersException.class)
	public ResponseEntity<Object> exception(PetPeersException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.CREATED);
	}

	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public ResponseEntity<Object> badRequest(MethodArgumentNotValidException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
	}

}
