package com.petpeersconsumer.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petpeersconsumer.exception.PetPeersException;
import com.petpeersconsumer.model.PetDTO;
import com.petpeersconsumer.model.User;
import com.petpeersconsumer.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author yerramreddy.sravani
 *
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetService petService;

	@Override
	@Transactional
	public User addUser(User user) {
		User user1 = null;
		String name = user.getUsername();
		String password = user.getUserPassword();
		String confimPassword = user.getConfirmPassword();

		if (name.equals(password)) {
			log.info("username and password must not be same");
			throw new PetPeersException("username and password must not be same");
		} else {
			if (password.equals(confimPassword)) {
				user1 = new User();
				user1.setUsername(user.getUsername());
				user1.setUserPassword(user.getUserPassword());

				userRepository.update(user.getId(), user.getUserPassword(), user.getPetId(), user.getUsername());
				User customer = user;
				return customer;
			} else {
				log.info("Password do no match");
				throw new PetPeersException("Password do no match");
			}
		}

	}

	@Override
	@Transactional
	public User updateUser(User user) {
		Optional<User> userFind = userRepository.findById(user.getId());
		User userUpdate = null;
		if (userFind != null) {
			User userNew = new User();
			userNew.setUsername(user.getUsername());
			userNew.setUserPassword(user.getUserPassword());
			userNew.setConfirmPassword(user.getConfirmPassword());
			Set<PetDTO> listPet = new HashSet<>();
			for (PetDTO pet : user.getPets()) {
				PetDTO pp = new PetDTO();
				pp.setName(pet.getName());
				pp.setAge(pet.getAge());
				pp.setPlace(pet.getPlace());
				pp.setPetOwnerId(pet.getPetOwnerId());
				listPet.add(pp);
			}
			userNew.setPets(listPet);

			userUpdate = userRepository.save(userNew);
		} else {
			userUpdate = null;
		}
		return userUpdate;
	}

	@Override
	@Transactional
	public List<User> listUsers() {

		return userRepository.findAll();
	}

	@Override
	@Transactional
	public User getUserById(int id) {

		return userRepository.getById(id);
	}

	@Override
	@Transactional
	public String removeUser(int id) {
		Optional<User> userFind = userRepository.findById(id);
		String info = null;
		if (userFind != null) {
			userRepository.deleteById(id);
			info = "user is deleted successfully...";
		} else {
			info = "user is not found";
		}
		return info;
	}

	@Override
	@Transactional
	public User findByUserName(String name) {

		return userRepository.findByUsername(name);
	}

	@Override
	@Transactional
	public User getMyPets(int userid) {

		return userRepository.getById(userid);
	}

	@Override
	@Transactional
	public User aunthenticateUser(String name, String password, String confimPassword) {
		User user = null;
		if (name != password) {
			if (password.equals(confimPassword)) {
				user = userRepository.findByUsernameAndUserPassword(name, password);
			} else {
				log.info("password and confimpassword should be same");
				throw new PetPeersException("password and confimpassword should be same");
			}
			return user;
		} else {
			log.info("username and password must not be same");
			throw new PetPeersException("username and password must not be same");
		}
	}

	@Override
	@Transactional
	public int saveUser(User user) {
		User user1 = null;
		String name = user.getUsername();
		String password = user.getUserPassword();
		String confimPassword = user.getConfirmPassword();

		if (name.equals(password)) {
			log.info("username and password must not be same");
			throw new PetPeersException("username and password must not be same");
		} else {
			if (password.equals(confimPassword)) {
				int id = 0;
				if (user != null) {
					user1 = new User();
					user1.setUsername(user.getUsername());
					user1.setUserPassword(user.getUserPassword());
					id = userRepository.save(user).getId();
				}
				return id;

			} else {
				log.info("Password do no match");
				throw new PetPeersException("Password do no match");
			}
		}

	}

	@Override
	@Transactional
	public PetDTO buyPet(int petId, int userId) {
		PetDTO petBought = null;
		Optional<User> userExists = userRepository.findById(userId);
		if (userExists != null) {
			PetDTO petPresent = petService.buyPet(petId);
			if (petPresent.getPetOwnerId() != 0) {
				throw new PetPeersException("Pet is already taken Try Another");
			} else {
				petBought = petPresent;
			}
		}
		return petBought;
	}

}
