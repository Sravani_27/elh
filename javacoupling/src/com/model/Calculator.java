
package com.model;

public class Calculator {

	// adding numbers
	public int add(int a, int b) {
		return a + b;

	}

	public int add(int a, int b, int c) {
		return a + b + c;

	}

	public float add(float a, int b) {
		return a + b;

	}

	public float add(int a, float b) {
		return a + b;

	}

	public float add(float a, float b) {
		return a + b;

	}

	// Subtraction of numbers

	public int sub(int a, int b) {
		return a - b;

	}

	public float sub(float a, int b) {
		return a - b;

	}

	public float sub(float a, float b) {
		return a - b;

	}

	public int sub(int a, int b, int c) {
		return a - b - c;

	}

	public double sub(int a, float b) {
		return a - b;
	}

	// multiplication of numbers
	public int mult(int a, int b) {
		return a * b;
	}

	public float mult(float a, int b) {
		return a * b;
	}

	public float mult(float a, float b) {
		return a * b;
	}

	public double mult(double a, float b) {
		return a * b;
	}

	// division of numbers
	public int div(int a, int b) {
		return a / b;
	}

	public float div(float a, int b) {
		return a / b;
	}

	public float div(int a, float b) {
		return a / b;
	}

	public float div(float a, float b) {
		return a / b;
	}

	public double mult(int a, double b) {
		return a / b;
		
	}

}
