package com.main;

import com.exercise.Box;

import com.model.Calculator;

public class CalculatorLaunch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// passing values
		Calculator calculator = new Calculator();

		System.out.println("addition of two numbers: "+calculator.add(10, 8));
		System.out.println("addition of 3 integers: "+calculator.add(10, 6, 289));

		System.out.println("substraction of two numbers: "+calculator.sub(80, 56));
		System.out.println("substraction of three numbers: "+calculator.sub(100, 6, 76));

		System.out.println("multiplication of two numbers: "+calculator.mult(80, 5));
		System.out.println("multiplication of int and float numbers: "+calculator.mult(700, 10.7));

		System.out.println("division of two numbers: "+calculator.div(305, 104));

		//Initializing values to variables 
		int x = 11, y = 54, z = 342;
		float q = 10.5f, r = 543.5f;
		double j= 100.5;

		// passing variables
		System.out.println("addition of two integers: "+calculator.add(x, y, z));
		System.out.println("addition of 3 numbers: "+calculator.add(y, z));

		System.out.println("addition of two numbers: "+calculator.sub(z, q));

		System.out.println("multiplication of two numbers: "+calculator.mult(j, q));

		System.out.println("division of two numbers: "+calculator.div(r, z));

		// calculating the volume of the box

		Box mybox = new Box(100, 43, 432);

		System.out.println("volume of the box is : " + mybox.volume());

		// creating another box object for passing the object reference having different
		// values

		Box mybox1 = new Box(mybox);
		// passing object reference

		double vol1 = mybox1.volume();

		System.out.println(vol1);

	}

}
