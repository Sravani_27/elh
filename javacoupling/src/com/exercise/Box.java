package com.exercise;

public class Box {
	double width, height, length;

	
	//passing object reference with values
	public Box(Box b) {
		width = b.width;
		height = b.height;
		length = b.length;

	}
	
	public Box(double width,double height, double length) {
		this.width = width;
		this.height = height;
		this.length = length;
	
	}
	

	public double volume() {
		return width * height * length;

	}
	
	

}
