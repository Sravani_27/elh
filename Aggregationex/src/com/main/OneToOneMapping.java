/**
 * @author yerramreddy.sravani
 */

package com.main;

import com.model.Department;
import com.model.Employee;

public class OneToOneMapping {

	public static void main(String[] args) {

		Employee employee1 = new Employee(324, "john", 2356f);
		Employee employee2 = new Employee(769, "vicky", 5400f);

		Department department = new Department();
		department.setEmployee(employee1);
		department.setDeptId(001);
		department.setDeptName("Development");
		System.out.println("Disaplay detaials: ");

		System.out.println(" " + department.getDeptId());
		System.out.println(" " + department.getDeptName());
		System.out.println(" " + department.getEmployee().getEmployeeId());
		System.out.println(" " + department.getEmployee().getEmployeeName());
		System.out.println(" " + department.getEmployee().getEmployeeSalary());

		department.setEmployee(employee2);
		department.setDeptId(002);
		department.setDeptName("Deployment");

		System.out.println("Disaplay detaials: ");
		System.out.println(" " + department.getDeptId());
		System.out.println(" " + department.getDeptName());
		System.out.println(" " + department.getEmployee().getEmployeeId());
		System.out.println(" " + department.getEmployee().getEmployeeName());
		System.out.println(" " + department.getEmployee().getEmployeeSalary());
		
		employee1 = null;
		employee2 = null;
		department = null;
		
	}
	

}
