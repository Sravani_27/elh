package com.demo3;
//<T> type of object
public class Employee {

	private int empNumber;
	private String empName;
	private float salary;

	public Employee() {
		super();
	}

	public Employee(int empNumber, String empName, float salary) {
		super();
		this.empNumber = empNumber;
		this.empName = empName;
		this.salary = salary;
	}

	public int getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(int empNumber) {
		this.empNumber = empNumber;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
