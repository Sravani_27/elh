package com.demo3;

import java.util.function.BiConsumer;

public class BiConsumerDemo {

	public static void main(String[] args) {
		BiConsumer<Integer, String> biConsumer = (Integer a, String b) -> {
		System.out.println("value of integer:"+ (a + 20));
		System.out.println("value of string: "+ b+" "+b.length());
		};
		biConsumer.accept(100, "Hello");
		System.out.println("End");
		}
	

	}


