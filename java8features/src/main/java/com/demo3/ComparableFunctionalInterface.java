package com.demo3;

public class ComparableFunctionalInterface {

	public static void main(String[] args) {
		Comparable<Employee> comparable = (what) -> {
			System.out.println("Inside what");
			// return 11;
			// return what.getEmpNumber() + 1000;
			return (int) what.getSalary() + 3000;
		};
		int ans = comparable.compareTo(new Employee(10, "Ten", 1010f));
		System.out.println(ans);
		System.out.println("End of the application");
	}
}
