package com.demo3;

import java.util.function.Function;

public class FunctionDemo {

	public static void main(String[] args) {
		Function<Employee, String> function = (a) -> {
			return "Welcome" + a.getEmpName();
		};
		String string = function.apply(new Employee(22, "Peacock", 2222.22f));
		System.out.println(string);
		System.out.println("The end");
	}

}
