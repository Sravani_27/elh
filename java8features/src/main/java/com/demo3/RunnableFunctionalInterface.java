package com.demo3;

public class RunnableFunctionalInterface {

	public static void main(String[] args) {

		Runnable runnable = () -> {
			System.out.println("Hi i'm Runnable interface");
		};
		runnable.run();

		System.out.println("The End...!");
	}

}
