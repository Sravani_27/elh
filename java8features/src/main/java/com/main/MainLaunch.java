package com.main;

import com.model.MyInterface;
import com.model.MyInterfaceImpl;

public class MainLaunch {

	public static void main(String[] args) {
		MyInterface myInterface = new MyInterfaceImpl();
		int ans = myInterface.add(2, 6);

		System.out.println("Addition:" + ans);
		System.out.println(MyInterface.PI);
		System.out.println("Substraction: " + myInterface.sub(10, 3));
		System.out.println("Multiplication: " + MyInterface.mul(2, 3));
	}

}
