package com.main;

import com.model.FunctionalInterfaceWithReturnAndArg;

public class LambdaWithArgAndReturn {

	public static void main(String[] args) {
		FunctionalInterfaceWithReturnAndArg andArg = (String name) -> {
			return "Welcome " + name;
		};
		String capture = andArg.sayHeloo("Sky!");
		System.out.println(capture);
		System.out.println("End of the Main()..!");
	}

}
