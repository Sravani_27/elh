package com.main;

import com.model.MyFunctionalInterfaceWithArg;

public class LambdaWithArgNoReturnType {

	public static void main(String[] args) {
		MyFunctionalInterfaceWithArg arg = (int var) -> {
			System.out.println("Lambda expression with argument and no return type: " + var);
		};
		arg.display(11001);
		System.out.println("End of Main()..!");
	}

}
