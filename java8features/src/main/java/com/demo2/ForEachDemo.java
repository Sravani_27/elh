package com.demo2;

import java.util.ArrayList;
import java.util.List;

public class ForEachDemo {

	public static void main(String[] args) {
		Department department1 = new Department(12, "Testing");
		Department department2 = new Department(14, "Develpment");
		Department department3 = new Department(20, "User interface");
		// collection == []

		List<Department> departments = new ArrayList();
		departments.add(department1);
		departments.add(department2);
		departments.add(department3);

		// iterate
		departments.forEach((abc) -> {
			System.out.println(abc.getDeptNo());
			System.out.println(abc.getDeptName());
		});
		
		
	}

}
