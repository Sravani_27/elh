package com.demo5;

public class InstanceMethodDemo {

	public void hello() {
		System.out.println("I am from World!");
	}

	public static void main(String[] args) {
		MyFunctionalInterface functionalInterface = () -> {
			System.out.println("With lambda expression");
		};
		functionalInterface.display();

		InstanceMethodDemo instanceMethodDemo = new InstanceMethodDemo();

		MyFunctionalInterface interface1 = instanceMethodDemo::hello;
		interface1.display();
		System.out.println("End");

	}

}
