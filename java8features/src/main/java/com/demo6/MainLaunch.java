package com.demo6;

public class MainLaunch {

	public static void main(String[] args) {
		MyStaticFunctionalInterface functionalInterface = (aa, bb) -> {
			return aa + bb;
		};

		System.out.println(functionalInterface.add(2, 22));

		// reference to a static method
		MyStaticFunctionalInterface functionalInterface2 = MyStaticClass::addTwoNumbers;
		int ans = functionalInterface2.add(11, 22);
		System.out.println(ans);
		System.out.println("The End");
	}

}
