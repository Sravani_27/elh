package com.demo6;

@FunctionalInterface
public interface MyStaticFunctionalInterface {

	public int add(int num1, int num2);
}
