package com.demo4;

import java.util.ArrayList;
import java.util.List;

public class SuperTypeDemo {

	public static void main(String[] args) {
		List<? super Number> list = new ArrayList();
		list.add(new Integer(10));
		list.add(new Float(12.3));
		
	}

}
