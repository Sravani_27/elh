package com.model;

public interface MyInterface {

	public static final float PI = 3.1415f;

	public static final int AGE = 18;

	public abstract int add(int a, int b);// method signature==prototype==method declaration

	public default int sub(int a, int b) {
		return a - b;
	}

	public default int div(int a, int b) {
		return a / b;
	}

	public static int mul(int a, int b) {
		return a * b;
	}
}
