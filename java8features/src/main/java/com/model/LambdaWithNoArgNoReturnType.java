package com.model;

public class LambdaWithNoArgNoReturnType {

	public static void main(String[] args) {

		MyFunctionalInterface myFunctionalInterface = () -> System.out.println("Welcome to lambda expression");
		myFunctionalInterface.display();

		LambdaWithNoArgNoReturnType.sayHello();
		LambdaWithNoArgNoReturnType.talkYourself(myFunctionalInterface);

	}

	public static void sayHello() { // class level
		System.out.println("Hello");
	}

	public static void talkYourself(MyFunctionalInterface var) {
		var.display();
		System.out.println("Talk yourself");
	}
}
