package com.model;

public interface AnotherInterface {
	public default int sub(int a, int b) {
		return a - b;
	}
}
