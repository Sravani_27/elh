package com.model;

public class MyInterfaceImpl implements MyInterface, AnotherInterface {

	@Override
	public int add(int a, int b) {

		return a + b;
	}

	@Override
	public int sub(int a, int b) {

		// return AnotherInterface.super.sub(a, b);
		return 12;
	}

	/*
	 * @Override public int sub(int a, int b) {
	 * 
	 * return MyInterface.super.sub(a, b); }
	 */

}
