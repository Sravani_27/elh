package com.main;

import com.model.Pen;
import com.model.Refill;

public class PenMain {

	public static void main(String[] args) {
		
		Refill refill = new Refill();
		refill.setBrand("writometer");
		refill.setColour("Blue");
		
		Pen pen = new Pen("Ball pen", 150, "Writometer", refill);
		
		System.out.println("Display details: ");
		System.out.println("pen brand is: "+ pen.getBrand());
		System.out.println("pen name is : "+ pen.getName());
		System.out.println("pen cost: " + pen.getPrice());
		System.out.println("pen's refill brand: "+ pen.getRefill().getBrand());
		System.out.println("pen's refill colour: "+ pen.getRefill().getColour());
		
		

	}

}
