package com.main;

import com.model.Charger;
import com.model.Mobile;
import com.model.OperatingSystem;

public class Launch {

	public static void main(String[] args) {

		Charger charger = new Charger("Samsung");
		OperatingSystem operatingSystem = new OperatingSystem();
		operatingSystem.setName("Android");
		operatingSystem.setSize(412);
		Mobile mobile = new Mobile("IMEI1000324567", "samsung", operatingSystem, charger);

		System.out.println(mobile.getOperatingSystem().getName());// Android
		System.out.println(mobile.getCharger().getBrand()); //samsung

		System.out.println(mobile.getImeiNo());

	}

}
