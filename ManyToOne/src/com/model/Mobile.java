package com.model;

public class Mobile {

	private String brand;
	private OperatingSystem operatingSystem;
	private Charger charger;

	public Mobile() {
		super();

	}

	private String imeiNo;

	public Mobile(String imeiNo, String brand, OperatingSystem operatingSystem, Charger charger) {
		super();
		this.imeiNo = imeiNo;
		this.brand = brand;
		this.operatingSystem = operatingSystem;
		this.charger = charger;
	}

	public Mobile(String imeiNo, String brand) {
		super();
		this.imeiNo = imeiNo;
		this.brand = brand;
	}

	public String getImeiNo() {
		return imeiNo;
	}

	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public OperatingSystem getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(OperatingSystem operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public Charger getCharger() {
		return charger;
	}

	public void setCharger(Charger charger) {
		this.charger = charger;
	}

}
