package com.model;

public class Pen {
	private String Name;
	private int price;
	private String brand;
	private Refill refill;

	public Pen() {
		super();

	}

	public Pen(String name, int price, String brand) {
		super();
		Name = name;
		this.price = price;
		this.brand = brand;
	}

	public Pen(String name, int price, String brand, Refill refill) {
		super();
		Name = name;
		this.price = price;
		this.brand = brand;
		this.refill = refill;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Refill getRefill() {
		return refill;
	}

	public void setRefill(Refill refill) {
		this.refill = refill;
	}

}
