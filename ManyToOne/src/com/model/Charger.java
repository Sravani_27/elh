package com.model;

public class Charger {

	private String brand;

	public Charger() {
		super();

	}

	public Charger(String brand) {
		super();
		this.brand = brand;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

}
