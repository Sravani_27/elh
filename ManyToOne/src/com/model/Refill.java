package com.model;

public class Refill {
	
	private String colour;
	private String brand;
	
	public Refill() {
		super();
		
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Refill(String colour, String brand) {
		super();
		this.colour = colour;
		this.brand = brand;
	}
	
	
	

}
